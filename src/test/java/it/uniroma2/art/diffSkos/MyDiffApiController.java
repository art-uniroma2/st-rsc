package it.uniroma2.art.diffSkos;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.NeededForDiff;
import it.uniroma2.art.diffSkos.stuctures.DiffResultStructure;
import it.uniroma2.art.diffSkos.stuctures.DiffTaskInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import it.uniroma2.art.utils.Utils.LexicalizationType;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static it.uniroma2.art.diffSkos.DiffTaskStructure.RESULT_FILE_NAME;

public class MyDiffApiController {
    private static final File maindDir = new File("diffTasks");

    public static ResponseEntity<String> executeDiffTask(NeededForDiff neededForDiff) throws IOException {
        String projectName1 = neededForDiff.getProjectName1();
        String versionRepoId1 = neededForDiff.getVersionRepoId1();
        String endPoint1 = neededForDiff.getSparqlEndpoint1();
        LexicalizationType lexicalizationType1 = LexicalizationType.fromValue(neededForDiff.getLexicalizationType1().toString());
        String username1 = neededForDiff.getUsername1();
        String password1 = neededForDiff.getPassword1();

        String projectName2 = neededForDiff.getProjectName2();
        String versionRepoId2 = neededForDiff.getVersionRepoId2();
        String endPoint2 = neededForDiff.getSparqlEndpoint2();
        String username2 = neededForDiff.getUsername2();
        String password2 = neededForDiff.getPassword2();

        LexicalizationType lexicalizationType2 = LexicalizationType.fromValue(neededForDiff.getLexicalizationType2().toString());
        List<String> langList = neededForDiff.getLangList();

        DiffTaskManager diffTaskManager = DiffTaskManager.getInstance(maindDir);
        String randId = diffTaskManager.createDiffStructAndStartTask(projectName1, versionRepoId1, endPoint1, lexicalizationType1, username1, password1,
                projectName2, versionRepoId2, endPoint2, lexicalizationType2,  username2, password2, langList);

        return new ResponseEntity<String>(randId, HttpStatus.OK);
    }

    public static ResponseEntity<Void> deleteDiffTask(String taskId) throws IOException {
        DiffTaskManager diffTaskManager = DiffTaskManager.getInstance(maindDir);
        if(diffTaskManager.getIdToDiffTaskStructureMap().containsKey(taskId)){
            diffTaskManager.getIdToDiffTaskStructureMap().remove(taskId);
            File taskDir = new File(maindDir, taskId);
            deleteDir(taskDir);
        } else {
            //the passed task id does not exist
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    private static void deleteDir(File fileOrDirFile) {
        if(fileOrDirFile.isDirectory()){
            for(File file : fileOrDirFile.listFiles()) {
                deleteDir(file);
            }
            //now that all files and dire have been deleted, delete the directory
            fileOrDirFile.delete();
        } else {
            //it is a file, so just delete it
            fileOrDirFile.delete();
        }
    }

    public static ResponseEntity<String> getDiffTaskResult(String taskId) throws IOException {
        File taskDir = new File(maindDir, taskId);
        if(!taskDir.exists() || !taskDir.isDirectory()) {
            throw new IOException("taskId is not a valid taskId");
        }
        File taskResultFile = new File(taskDir, RESULT_FILE_NAME);
        if(!taskResultFile.exists()) {
            throw new IOException("taskId has no results");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        DiffResultStructure diffResultStructure = objectMapper.readValue(taskResultFile, DiffResultStructure.class);
        String jsonResult = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(diffResultStructure);

        return new ResponseEntity<>(jsonResult, HttpStatus.OK);

    }

    public static ResponseEntity<String> tasksInfo(String projectName) throws IOException {
        boolean applyFilter = false;
        if(projectName!=null && !projectName.isEmpty()){
            applyFilter = true;
        }
        List<DiffTaskInfo> diffTaskInfoList = new ArrayList<>();
        DiffTaskManager diffTaskManager = DiffTaskManager.getInstance(maindDir);
        Map<String, DiffTaskStructure> taskManagerIdToDiffTaskStructureMap = diffTaskManager.getIdToDiffTaskStructureMap();
        for(String taskId : taskManagerIdToDiffTaskStructureMap.keySet()){
            DiffTaskStructure diffTaskStructure = taskManagerIdToDiffTaskStructureMap.get(taskId);
            if(applyFilter){
                if(!diffTaskStructure.getTaskInfo().getLeftDataset().getProjectName().equals(projectName)){
                    //it is a different project than the one passed, so just skip it
                    continue;
                }
            }
            diffTaskInfoList.add(diffTaskStructure.getTaskInfo());
        }

        //transform the list of TaskInfo into a json
        ObjectMapper objectMapper = new ObjectMapper();
        String taskInfoListString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(diffTaskInfoList);

        return new ResponseEntity<>(taskInfoListString, HttpStatus.OK);
    }


}

