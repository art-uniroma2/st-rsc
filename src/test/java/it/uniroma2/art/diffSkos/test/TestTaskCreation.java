package it.uniroma2.art.diffSkos.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.uniroma2.art.diffSkos.DiffTaskManager;
import it.uniroma2.art.diffSkos.DiffTaskStructure;
import it.uniroma2.art.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TestTaskCreation {

    public static void main(String[] args) {
        TestTaskCreation testClass = new TestTaskCreation();
        try {
            testClass.startTest();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("PROGRAM TERMINATED");
    }

    private void startTest() throws IOException, InterruptedException {
        String endpoint1, endpoint2, projectName1, projectName2, versionRepoId1, versionRepoId2, username1, username2, password1, password2;
        List<String> langList = new ArrayList();
        Utils.LexicalizationType lexicalization1Type;
        Utils.LexicalizationType lexicalization2Type;

        File maindDir = new File("tasks");

        DiffTaskManager diffTaskManager = DiffTaskManager.getInstance(maindDir);


        endpoint1 = "http://127.0.0.1:7200/repositories/DiffProj1_core";
        endpoint2 = "http://127.0.0.1:7200/repositories/DiffProj2_core";
        langList.clear();
        langList.add("en");
        lexicalization1Type = Utils.LexicalizationType.SKOS_XL;
        lexicalization2Type = Utils.LexicalizationType.SKOS_XL;
        projectName1 = "project1";
        projectName2 = "project2";
        versionRepoId1 = null;
        versionRepoId2 = null;
        username1 = "testU";
        password1 = "testP";
        username2 = "testU";
        password2 = "testP";

        String randId = diffTaskManager.createDiffStructAndStartTask(projectName1, versionRepoId1, endpoint1, lexicalization1Type,
                username1, password1,
                projectName2, versionRepoId2, endpoint2, lexicalization2Type, username2, password2, langList);

        //endpoint1 = "http://agrovoc2.it";
        //endpoint2 = "http://eurovoc2.it";
        //diffTaskManager.createDiffStruct(endpoint1, endpoint2);

        boolean allTaskTerminated = false;
        while(!allTaskTerminated){
            allTaskTerminated = true;
            Map<String, DiffTaskStructure> idToDiffTaskStructureMap = diffTaskManager.getIdToDiffTaskStructureMap();
            System.out.println("");
            for(String taskId : idToDiffTaskStructureMap.keySet()){
                String status = idToDiffTaskStructureMap.get(taskId).getTaskInfo().getStatus();
                if(status.equals(Utils.TaskStatus.execution.name())){
                    allTaskTerminated = false;
                }
                System.out.println("task: "+taskId+" -> status: "+status);
            }
            Thread.sleep(2000);
        }

        DiffTaskStructure diffTaskStructure = diffTaskManager.getDiffStructFromTaskId(randId);
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResult = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(diffTaskStructure);
        System.out.println("jsonResult:\n"+jsonResult);

        System.out.println("TEST METHOD TERMINATED");
        System.exit(0);
    }


}
