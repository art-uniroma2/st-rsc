package it.uniroma2.art.diffSkos;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.uniroma2.art.utils.Utils;
import it.uniroma2.art.utils.Utils.LexicalizationType;
import it.uniroma2.art.diffSkos.stuctures.DiffTaskInfo;

import static it.uniroma2.art.diffSkos.DiffTaskStructure.CONFIG_JSON_FILE_NAME;


public class DiffTaskManager {

    private static DiffTaskManager diffTaskManager = null;

    private Map<String, DiffTaskStructure> idToDiffTaskStructureMap = new HashMap<>();
    private File maindDir;

    private DiffTaskManager(File mainDir) throws IOException {
        this.maindDir = mainDir;
        if(!mainDir.exists()){
            mainDir.mkdirs();
        }
        
        //from the maindDir get all the directory and each directory name represents a previously executed diffTask, so read from it the configuration parameters
        getPrevioslyDiffTasks();
    }

    public static DiffTaskManager getInstance(File mainDir) throws IOException {
        if(diffTaskManager == null) {
            diffTaskManager = new DiffTaskManager(mainDir);
        } return diffTaskManager;
    }

    private void getPrevioslyDiffTasks() throws IOException {
        for(File taskDir : maindDir.listFiles()){
            if(!taskDir.isDirectory()){
                //it is not a directory, so skip it
                continue;
            }
            //search for the file CONFIG_FILE_NAME
            File configFile = new File(taskDir, CONFIG_JSON_FILE_NAME);
            if(!configFile.exists()){
                //there is no config file in this taskDir, so skip it
                continue;
            }
            //read the configuration file, written in json, and create the DiffTaskStructure
            ObjectMapper objectMapper = new ObjectMapper();
            DiffTaskInfo diffTaskInfo = objectMapper.readValue(configFile, DiffTaskInfo.class);
            DiffTaskStructure diffTaskStructure = new DiffTaskStructure(diffTaskInfo, maindDir);
            //if any taskInfo has a value "execution" for status, set it to "error"
            if(diffTaskStructure.getTaskInfo().getStatus().equals(Utils.TaskStatus.execution.name())){
                diffTaskStructure.updateStatus(Utils.TaskStatus.error);
                diffTaskStructure.saveTaskInfo();
            }
            idToDiffTaskStructureMap.put(diffTaskInfo.getTaskId(), diffTaskStructure);
        }
    }

    public String createDiffStructAndStartTask(String projectName1, String versionRepoId1, String sparqlEndpoint1, LexicalizationType lexicalization1,
            String username1, String password1,
            String projectName2, String versionRepoId2, String sparqlEndpoint2, LexicalizationType lexicalization2, String username2, String password2,
            List<String> langs) throws IOException {
        String randId;
        randId = Utils.getInstance().generateRandId(sparqlEndpoint1, sparqlEndpoint2, Lists.newArrayList(idToDiffTaskStructureMap.keySet()));
        DiffTaskStructure diffTaskStructure = new DiffTaskStructure(projectName1, versionRepoId1, sparqlEndpoint1, lexicalization1, username1, password1,
                projectName2, versionRepoId2, sparqlEndpoint2, lexicalization2, username2, password2,
                langs, randId, "", maindDir);
        idToDiffTaskStructureMap.put(randId, diffTaskStructure);
        diffTaskStructure.startDiffTask();
        return randId;
    }

    public Map<String, DiffTaskStructure> getIdToDiffTaskStructureMap(){
        return idToDiffTaskStructureMap;
    }

    public DiffTaskStructure getDiffStructFromTaskId(String taskId){
        return idToDiffTaskStructureMap.get(taskId);
    }


}
