package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;

/**
 * AlignmentChain
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-23T17:07:40.535Z[GMT]")


public class AlignmentChain   {
  @JsonProperty("score")
  private Double score = null;

  @JsonProperty("chain")
  @Valid
  private List<String> chain = new ArrayList<String>();

  public AlignmentChain score(Double score) {
    this.score = score;
    return this;
  }

  /**
   * the score of this alignment chain
   * @return score
   **/
  @Schema(required = true, description = "the score of this alignment chain")
      @NotNull

    public Double getScore() {
    return score;
  }

  public void setScore(Double score) {
    this.score = score;
  }

  public AlignmentChain chain(List<String> chain) {
    this.chain = chain;
    return this;
  }

  public AlignmentChain addChainItem(String chainItem) {
    this.chain.add(chainItem);
    return this;
  }

  /**
   * references to the alignments in this chain
   * @return chain
   **/
  @Schema(required = true, description = "references to the alignments in this chain")
      @NotNull

    public List<String> getChain() {
    return chain;
  }

  public void setChain(List<String> chain) {
    this.chain = chain;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AlignmentChain alignmentChain = (AlignmentChain) o;
    return Objects.equals(this.score, alignmentChain.score) &&
        Objects.equals(this.chain, alignmentChain.chain);
  }

  @Override
  public int hashCode() {
    return Objects.hash(score, chain);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AlignmentChain {\n");
    
    sb.append("    score: ").append(toIndentedString(score)).append("\n");
    sb.append("    chain: ").append(toIndentedString(chain)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
