package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;

/**
 * NeededForDiff
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-23T17:07:40.535Z[GMT]")


public class NeededForDiff   {
  @JsonProperty("sparqlEndpoint1")
  private String sparqlEndpoint1 = null;

  /**
   * Gets or Sets lexicalizationType1
   */
  public enum LexicalizationType1Enum {
    _2004_02_SKOS_CORE("http://www.w3.org/2004/02/skos/core"),
    
    _2008_05_SKOS_XL("http://www.w3.org/2008/05/skos-xl");

    private String value;

    LexicalizationType1Enum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static LexicalizationType1Enum fromValue(String text) {
      for (LexicalizationType1Enum b : LexicalizationType1Enum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }
  @JsonProperty("lexicalizationType1")
  private LexicalizationType1Enum lexicalizationType1 = null;

  @JsonProperty("projectName1")
  private String projectName1 = null;

  @JsonProperty("versionRepoId1")
  private String versionRepoId1 = null;

  @JsonProperty("username1")
  private String username1 = null;

  @JsonProperty("password1")
  private String password1 = null;

  @JsonProperty("sparqlEndpoint2")
  private String sparqlEndpoint2 = null;

  /**
   * Gets or Sets lexicalizationType2
   */
  public enum LexicalizationType2Enum {
    _2004_02_SKOS_CORE("http://www.w3.org/2004/02/skos/core"),
    
    _2008_05_SKOS_XL("http://www.w3.org/2008/05/skos-xl");

    private String value;

    LexicalizationType2Enum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static LexicalizationType2Enum fromValue(String text) {
      for (LexicalizationType2Enum b : LexicalizationType2Enum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }
  @JsonProperty("lexicalizationType2")
  private LexicalizationType2Enum lexicalizationType2 = null;

  @JsonProperty("projectName2")
  private String projectName2 = null;

  @JsonProperty("versionRepoId2")
  private String versionRepoId2 = null;

  @JsonProperty("username2")
  private String username2 = null;

  @JsonProperty("password2")
  private String password2 = null;

  @JsonProperty("langList")
  @Valid
  private List<String> langList = null;

  public NeededForDiff sparqlEndpoint1(String sparqlEndpoint1) {
    this.sparqlEndpoint1 = sparqlEndpoint1;
    return this;
  }

  /**
   * Get sparqlEndpoint1
   * @return sparqlEndpoint1
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getSparqlEndpoint1() {
    return sparqlEndpoint1;
  }

  public void setSparqlEndpoint1(String sparqlEndpoint1) {
    this.sparqlEndpoint1 = sparqlEndpoint1;
  }

  public NeededForDiff lexicalizationType1(LexicalizationType1Enum lexicalizationType1) {
    this.lexicalizationType1 = lexicalizationType1;
    return this;
  }

  /**
   * Get lexicalizationType1
   * @return lexicalizationType1
   **/
  @Schema(required = true, description = "")
      @NotNull

    public LexicalizationType1Enum getLexicalizationType1() {
    return lexicalizationType1;
  }

  public void setLexicalizationType1(LexicalizationType1Enum lexicalizationType1) {
    this.lexicalizationType1 = lexicalizationType1;
  }

  public NeededForDiff projectName1(String projectName1) {
    this.projectName1 = projectName1;
    return this;
  }

  /**
   * Get projectName1
   * @return projectName1
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getProjectName1() {
    return projectName1;
  }

  public void setProjectName1(String projectName1) {
    this.projectName1 = projectName1;
  }

  public NeededForDiff versionRepoId1(String versionRepoId1) {
    this.versionRepoId1 = versionRepoId1;
    return this;
  }

  /**
   * Get versionRepoId1
   * @return versionRepoId1
   **/
  @Schema(description = "")
  
    public String getVersionRepoId1() {
    return versionRepoId1;
  }

  public void setVersionRepoId1(String versionRepoId1) {
    this.versionRepoId1 = versionRepoId1;
  }

  public NeededForDiff username1(String username1) {
    this.username1 = username1;
    return this;
  }

  /**
   * Get username1
   * @return username1
   **/
  @Schema(description = "")
  
    public String getUsername1() {
    return username1;
  }

  public void setUsername1(String username1) {
    this.username1 = username1;
  }

  public NeededForDiff password1(String password1) {
    this.password1 = password1;
    return this;
  }

  /**
   * Get password1
   * @return password1
   **/
  @Schema(description = "")
  
    public String getPassword1() {
    return password1;
  }

  public void setPassword1(String password1) {
    this.password1 = password1;
  }

  public NeededForDiff sparqlEndpoint2(String sparqlEndpoint2) {
    this.sparqlEndpoint2 = sparqlEndpoint2;
    return this;
  }

  /**
   * Get sparqlEndpoint2
   * @return sparqlEndpoint2
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getSparqlEndpoint2() {
    return sparqlEndpoint2;
  }

  public void setSparqlEndpoint2(String sparqlEndpoint2) {
    this.sparqlEndpoint2 = sparqlEndpoint2;
  }

  public NeededForDiff lexicalizationType2(LexicalizationType2Enum lexicalizationType2) {
    this.lexicalizationType2 = lexicalizationType2;
    return this;
  }

  /**
   * Get lexicalizationType2
   * @return lexicalizationType2
   **/
  @Schema(required = true, description = "")
      @NotNull

    public LexicalizationType2Enum getLexicalizationType2() {
    return lexicalizationType2;
  }

  public void setLexicalizationType2(LexicalizationType2Enum lexicalizationType2) {
    this.lexicalizationType2 = lexicalizationType2;
  }

  public NeededForDiff projectName2(String projectName2) {
    this.projectName2 = projectName2;
    return this;
  }

  /**
   * Get projectName2
   * @return projectName2
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getProjectName2() {
    return projectName2;
  }

  public void setProjectName2(String projectName2) {
    this.projectName2 = projectName2;
  }

  public NeededForDiff versionRepoId2(String versionRepoId2) {
    this.versionRepoId2 = versionRepoId2;
    return this;
  }

  /**
   * Get versionRepoId2
   * @return versionRepoId2
   **/
  @Schema(description = "")
  
    public String getVersionRepoId2() {
    return versionRepoId2;
  }

  public void setVersionRepoId2(String versionRepoId2) {
    this.versionRepoId2 = versionRepoId2;
  }

  public NeededForDiff username2(String username2) {
    this.username2 = username2;
    return this;
  }

  /**
   * Get username2
   * @return username2
   **/
  @Schema(description = "")
  
    public String getUsername2() {
    return username2;
  }

  public void setUsername2(String username2) {
    this.username2 = username2;
  }

  public NeededForDiff password2(String password2) {
    this.password2 = password2;
    return this;
  }

  /**
   * Get password2
   * @return password2
   **/
  @Schema(description = "")
  
    public String getPassword2() {
    return password2;
  }

  public void setPassword2(String password2) {
    this.password2 = password2;
  }

  public NeededForDiff langList(List<String> langList) {
    this.langList = langList;
    return this;
  }

  public NeededForDiff addLangListItem(String langListItem) {
    if (this.langList == null) {
      this.langList = new ArrayList<String>();
    }
    this.langList.add(langListItem);
    return this;
  }

  /**
   * Get langList
   * @return langList
   **/
  @Schema(description = "")
  
    public List<String> getLangList() {
    return langList;
  }

  public void setLangList(List<String> langList) {
    this.langList = langList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NeededForDiff neededForDiff = (NeededForDiff) o;
    return Objects.equals(this.sparqlEndpoint1, neededForDiff.sparqlEndpoint1) &&
        Objects.equals(this.lexicalizationType1, neededForDiff.lexicalizationType1) &&
        Objects.equals(this.projectName1, neededForDiff.projectName1) &&
        Objects.equals(this.versionRepoId1, neededForDiff.versionRepoId1) &&
        Objects.equals(this.username1, neededForDiff.username1) &&
        Objects.equals(this.password1, neededForDiff.password1) &&
        Objects.equals(this.sparqlEndpoint2, neededForDiff.sparqlEndpoint2) &&
        Objects.equals(this.lexicalizationType2, neededForDiff.lexicalizationType2) &&
        Objects.equals(this.projectName2, neededForDiff.projectName2) &&
        Objects.equals(this.versionRepoId2, neededForDiff.versionRepoId2) &&
        Objects.equals(this.username2, neededForDiff.username2) &&
        Objects.equals(this.password2, neededForDiff.password2) &&
        Objects.equals(this.langList, neededForDiff.langList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sparqlEndpoint1, lexicalizationType1, projectName1, versionRepoId1, username1, password1, sparqlEndpoint2, lexicalizationType2, projectName2, versionRepoId2, username2, password2, langList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NeededForDiff {\n");
    
    sb.append("    sparqlEndpoint1: ").append(toIndentedString(sparqlEndpoint1)).append("\n");
    sb.append("    lexicalizationType1: ").append(toIndentedString(lexicalizationType1)).append("\n");
    sb.append("    projectName1: ").append(toIndentedString(projectName1)).append("\n");
    sb.append("    versionRepoId1: ").append(toIndentedString(versionRepoId1)).append("\n");
    sb.append("    username1: ").append(toIndentedString(username1)).append("\n");
    sb.append("    password1: ").append(toIndentedString(password1)).append("\n");
    sb.append("    sparqlEndpoint2: ").append(toIndentedString(sparqlEndpoint2)).append("\n");
    sb.append("    lexicalizationType2: ").append(toIndentedString(lexicalizationType2)).append("\n");
    sb.append("    projectName2: ").append(toIndentedString(projectName2)).append("\n");
    sb.append("    versionRepoId2: ").append(toIndentedString(versionRepoId2)).append("\n");
    sb.append("    username2: ").append(toIndentedString(username2)).append("\n");
    sb.append("    password2: ").append(toIndentedString(password2)).append("\n");
    sb.append("    langList: ").append(toIndentedString(langList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
