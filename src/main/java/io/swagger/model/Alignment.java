package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.model.DataService;
import io.swagger.model.Dataset;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;

/**
 * Alignment
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-23T17:07:40.535Z[GMT]")


public class Alignment extends Dataset  {
  @JsonProperty("subjectsTarget")
  private String subjectsTarget = null;

  @JsonProperty("objectsTarget")
  private String objectsTarget = null;

  public Alignment subjectsTarget(String subjectsTarget) {
    this.subjectsTarget = subjectsTarget;
    return this;
  }

  /**
   * Get subjectsTarget
   * @return subjectsTarget
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getSubjectsTarget() {
    return subjectsTarget;
  }

  public void setSubjectsTarget(String subjectsTarget) {
    this.subjectsTarget = subjectsTarget;
  }

  public Alignment objectsTarget(String objectsTarget) {
    this.objectsTarget = objectsTarget;
    return this;
  }

  /**
   * Get objectsTarget
   * @return objectsTarget
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getObjectsTarget() {
    return objectsTarget;
  }

  public void setObjectsTarget(String objectsTarget) {
    this.objectsTarget = objectsTarget;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Alignment alignment = (Alignment) o;
    return Objects.equals(this.subjectsTarget, alignment.subjectsTarget) &&
        Objects.equals(this.objectsTarget, alignment.objectsTarget) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(subjectsTarget, objectsTarget, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Alignment {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    subjectsTarget: ").append(toIndentedString(subjectsTarget)).append("\n");
    sb.append("    objectsTarget: ").append(toIndentedString(objectsTarget)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
