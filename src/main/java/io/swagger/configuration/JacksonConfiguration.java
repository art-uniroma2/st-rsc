package io.swagger.configuration;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.openapitools.jackson.nullable.JsonNullableModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JacksonConfiguration {

  @Bean(name = "io.swagger.configuration.JacksonConfiguration.jsonNullableModule")
  public Module jsonNullableModule() {
    return new JsonNullableModule();
  }

  @Bean(name = "io.swagger.configuration.JacksonConfiguration.javaTimeModule")
  public Module javaTimeModule() {
    return new JavaTimeModule();
  }
}
