package io.swagger.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringDocConfiguration {

    @Bean(name = "io.swagger.configuration.SpringDocConfiguration.apiInfo")
    OpenAPI apiInfo() {
        return new OpenAPI()
                .info(
                        new Info()
                                .title("SKOS Diffing system")
                                .description("This is a server performing a SKOS diffing system and Bootstrapping alignments.")
                                .termsOfService("")
                                .version("1.1.0")
                                .license(new License()
                                        .name("Apache 2.0")
                                        .url("http://www.apache.org/licenses/LICENSE-2.0.html"))
                                .contact(new io.swagger.v3.oas.models.info.Contact()
                                        .email("turbati@info.uniroma2.it")));
    }
}