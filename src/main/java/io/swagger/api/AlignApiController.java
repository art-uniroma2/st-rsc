package io.swagger.api;

import io.swagger.model.AlignmentPlan;
import io.swagger.model.Matcher;
import it.uniroma2.art.align.MyAlignApiController;
import org.springframework.core.io.Resource;
import io.swagger.model.ServiceMetadata;
import io.swagger.model.Task;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import jakarta.validation.Valid;
import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-23T17:07:40.535Z[GMT]")
@RestController
public class AlignApiController implements AlignApi {

    private static final Logger log = LoggerFactory.getLogger(AlignApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public AlignApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> deleteTaskByID(@Parameter(in = ParameterIn.PATH, description = "Task ID", required=true, schema=@Schema()) @PathVariable("id") String id) {
        String accept = request.getHeader("Accept");
        try {
            return MyAlignApiController.deleteDiffTask(id);
        } catch (IOException ioException) {
            ioException.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Resource> downloadAlignment(@Parameter(in = ParameterIn.PATH, description = "Task ID", required=true, schema=@Schema()) @PathVariable("id") String id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/rdf+xml")) {
            try {
                return MyAlignApiController.getMatchTaskResult(id);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Resource>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Resource>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Matcher> getMatcherByID(@Parameter(in = ParameterIn.PATH, description = "Matcher ID", required=true, schema=@Schema()) @PathVariable("id") String id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Matcher>(objectMapper.readValue("{}", Matcher.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Matcher>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Matcher>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Matcher>> getMatchers() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Matcher>>(objectMapper.readValue("[ {\n  \"id\" : \"example-matcher\",\n  \"description\" : \"example matcher\",\n  \"settings\" : {\n    \"type\" : \"object\",\n    \"properties\" : {\n      \"structuralFeatures\" : {\n        \"description\" : \"whether to use structural features or not\",\n        \"type\" : \"boolean\",\n        \"default\" : true\n      },\n      \"synonymExpansion\" : {\n        \"description\" : \"whether to do synonym expansion or not\",\n        \"type\" : \"boolean\",\n        \"default\" : true\n      }\n    }\n  }\n}, {\n  \"id\" : \"example-matcher\",\n  \"description\" : \"example matcher\",\n  \"settings\" : {\n    \"type\" : \"object\",\n    \"properties\" : {\n      \"structuralFeatures\" : {\n        \"description\" : \"whether to use structural features or not\",\n        \"type\" : \"boolean\",\n        \"default\" : true\n      },\n      \"synonymExpansion\" : {\n        \"description\" : \"whether to do synonym expansion or not\",\n        \"type\" : \"boolean\",\n        \"default\" : true\n      }\n    }\n  }\n} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Matcher>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Matcher>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<ServiceMetadata> getServiceMetadata() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return MyAlignApiController.getServiceMetadata();
        }

        return new ResponseEntity<ServiceMetadata>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Task> getTaskByID(@Parameter(in = ParameterIn.PATH, description = "Task ID", required=true, schema=@Schema()) @PathVariable("id") String id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return MyAlignApiController.taskInfoFromId(id);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Task>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Task>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Task>> getTasks() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return MyAlignApiController.tasksInfo();
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Task>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Task>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Matcher>> searchMatchers() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Matcher>>(objectMapper.readValue("{}", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Matcher>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Matcher>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Task> submitTask(@Parameter(in = ParameterIn.DEFAULT, description = "All the values needed for the SKOS Diffing task", required=true, schema=@Schema()) @Valid @RequestBody AlignmentPlan body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return MyAlignApiController.executeDiffTask(body);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Task>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Task>(HttpStatus.NOT_IMPLEMENTED);
    }

}
