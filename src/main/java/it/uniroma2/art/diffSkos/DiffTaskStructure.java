package it.uniroma2.art.diffSkos;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.uniroma2.art.diffSkos.stuctures.DatasetInfo;
import it.uniroma2.art.diffSkos.stuctures.DiffResultStructure;
import it.uniroma2.art.diffSkos.stuctures.DiffTaskInfo;
import it.uniroma2.art.diffSkos.stuctures.TaskExecutionInfo;
import it.uniroma2.art.utils.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

public class DiffTaskStructure {

    //public static String CONFIG_FILE_NAME = "task.properties";
    public static String CONFIG_JSON_FILE_NAME = "task.json";
    public static String RESULT_FILE_NAME = "result.json";
    public static String TASK_EXEC_FILE_NAME = "taskExecution.json";


    private final DiffTaskInfo diffTaskInfo;

    private TaskExecutionInfo taskExecutionInfo;

    private final File mainDir;

    public DiffTaskStructure(DiffTaskInfo diffTaskInfo, File mainDir) {
        this.diffTaskInfo = diffTaskInfo;
        this.mainDir = mainDir;
    }

    public DiffTaskStructure(
            String projectName1, String versionRepoId1, String sparqlEndpoint1, Utils.LexicalizationType lexicalizationType1,
            String username1, String password1,
            String projectName2, String versionRepoId2, String sparqlEndpoint2, Utils.LexicalizationType lexicalizationType2,
            String username2, String password2,
            List<String> langs, String taskId, String exectutionTime, File mainDir) {
        DatasetInfo leftDataset = new DatasetInfo(projectName1, versionRepoId1, sparqlEndpoint1, lexicalizationType1.getValue(), username1, password1);
        DatasetInfo rightDataset = new DatasetInfo(projectName2, versionRepoId2, sparqlEndpoint2, lexicalizationType2.getValue(), username2, password2);
        diffTaskInfo = new DiffTaskInfo(leftDataset, rightDataset, taskId, exectutionTime, "", langs);
        taskExecutionInfo = new TaskExecutionInfo();
        this.mainDir = mainDir;
    }

    public DiffTaskInfo getTaskInfo() {
        return diffTaskInfo;
    }

    public TaskExecutionInfo getTaskExecutionInfo() {
        return taskExecutionInfo;
    }

    public File getMainDir() {
        return mainDir;
    }


    public void startDiffTask() throws IOException {
        diffTaskInfo.setStatus(Utils.TaskStatus.execution.name());
        File taskDir = new File(mainDir, diffTaskInfo.getTaskId());
        taskDir.mkdirs();
        saveTaskInfo();
        //addConfigFile();
        DiffTaskThread diffTaskThread = new DiffTaskThread(this);
        diffTaskThread.start();
    }

    public void updateStatus(Utils.TaskStatus newStatus) throws IOException {
        diffTaskInfo.setStatus(newStatus.name());
        saveTaskInfo();
    }

    public void updateExecutionTime(String exectutionTime) throws IOException {
        diffTaskInfo.setExecutionTime(exectutionTime);
        saveTaskInfo();
    }

    private File getConfigFile(){
        File taskDir = new File(mainDir, diffTaskInfo.getTaskId());
        return new File(taskDir, CONFIG_JSON_FILE_NAME);
    }

    public void saveTaskInfo() throws IOException {
        ObjectMapper obj = new ObjectMapper();
        String jsonString = obj.writerWithDefaultPrettyPrinter().writeValueAsString(diffTaskInfo);
        File taskDir = new File(mainDir, diffTaskInfo.getTaskId());
        File outputFile = new File(taskDir, CONFIG_JSON_FILE_NAME);

        try (OutputStream os = Files.newOutputStream(outputFile.toPath());
             OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
             BufferedWriter bf = new BufferedWriter(osw)) {
            bf.write(jsonString);
            bf.flush();
        }
    }

    public void saveTaskExecutionInfo() throws  IOException {
        ObjectMapper obj = new ObjectMapper();
        String jsonString = obj.writerWithDefaultPrettyPrinter().writeValueAsString(taskExecutionInfo);
        File taskDir = new File(mainDir, diffTaskInfo.getTaskId());
        File outputFile = new File(taskDir, TASK_EXEC_FILE_NAME);

        try (OutputStream os = Files.newOutputStream(outputFile.toPath());
             OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
             BufferedWriter bf = new BufferedWriter(osw)) {
            bf.write(jsonString);
            bf.flush();
        }
    }

    public void saveResult(DiffResultStructure diffResultStructure) throws IOException {
        ObjectMapper obj = new ObjectMapper();
        String jsonString = obj.writerWithDefaultPrettyPrinter().writeValueAsString(diffResultStructure);
        File taskDir = new File(mainDir, diffTaskInfo.getTaskId());
        File outputFile = new File(taskDir, RESULT_FILE_NAME);

        try (OutputStream os = Files.newOutputStream(outputFile.toPath());
             OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
             BufferedWriter bf = new BufferedWriter(osw)) {
            bf.write(jsonString);
            bf.flush();
        }
    }

}
