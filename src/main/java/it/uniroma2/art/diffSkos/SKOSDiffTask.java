package it.uniroma2.art.diffSkos;

import it.uniroma2.art.diffSkos.stuctures.ChangedLabel;
import it.uniroma2.art.diffSkos.stuctures.ChangedResource;
import it.uniroma2.art.diffSkos.stuctures.DatasetInfo;
import it.uniroma2.art.diffSkos.stuctures.DiffResultStructure;
import it.uniroma2.art.diffSkos.stuctures.LabelWithInfo;
import it.uniroma2.art.diffSkos.stuctures.LabelWithResAndLitForm;
import it.uniroma2.art.diffSkos.stuctures.ResourceWithInfo;
import it.uniroma2.art.diffSkos.stuctures.ResourceWithLexicalization;
import it.uniroma2.art.utils.Utils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SKOSDiffTask {

    private final int NUM_RES_PER_QUERY = 30;
    private final int ITERATION_WHEN_TO_SAVE = 5;

    private DiffTaskStructure diffTaskStructure;

    private SPARQLRepository sparqlSelectRepository1;
    private SPARQLRepository sparqlSelectRepository2;

    //maps/list about skos:Concept, skos:ConceptScheme and skos:Collection (and their subclasses)
    private List<ResourceWithLexicalization> addedResourceList = new ArrayList<>();
    private List<ResourceWithLexicalization> removedResourceList = new ArrayList<>();
    private HashMap<String, ResourceWithLexicalization> resourcesInCommonMap = new HashMap<>(); //this stores just the resources in common, not their differences
    private List<ChangedResource> changedResourceList = new ArrayList<>();

    //maps about skosxl:Label (and their subclasses), used when both lexicalization types are SKOSXL
    private List<LabelWithResAndLitForm> addedLabelList = new ArrayList<>();
    private List<LabelWithResAndLitForm> removedLabelList = new ArrayList<>();
    private HashMap<String, LabelWithResAndLitForm> labelsInCommonMap = new HashMap<>(); //this store just the labels in common, not their differences
    private List<ChangedLabel> changedLabelList = new ArrayList<>();


    public SKOSDiffTask(DiffTaskStructure diffTaskStructure) {
        this.diffTaskStructure = diffTaskStructure;
    }

    public void startDiffTask() throws IOException {
        DatasetInfo leftDatasetInfo = diffTaskStructure.getTaskInfo().getLeftDataset();
        sparqlSelectRepository1 = new SPARQLRepository(leftDatasetInfo.getSparqlEndpoint());
        if(leftDatasetInfo.getUsername() != null && leftDatasetInfo.getPassword()!=null) {
            sparqlSelectRepository1.setUsernameAndPassword(leftDatasetInfo.getUsername(), leftDatasetInfo.getPassword());
        }
        sparqlSelectRepository1.initialize();

        DatasetInfo rightDatasetInfo = diffTaskStructure.getTaskInfo().getRightDataset();
        sparqlSelectRepository2 = new SPARQLRepository(rightDatasetInfo.getSparqlEndpoint());
        if(rightDatasetInfo.getUsername() != null && rightDatasetInfo.getPassword()!=null) {
            sparqlSelectRepository2.setUsernameAndPassword(rightDatasetInfo.getUsername(), rightDatasetInfo.getPassword());

        }
        sparqlSelectRepository2.initialize();

        //get all resources from connection 1
        HashMap<String, ResourceWithLexicalization> resourcesInRepository1Map =
                getResourceMap(sparqlSelectRepository1, Utils.LexicalizationType.fromValue(diffTaskStructure.getTaskInfo().getLeftDataset().getLexicalizationIRI()),
                        diffTaskStructure.getTaskInfo().getLangsShown());
        diffTaskStructure.getTaskExecutionInfo().setResourcesLeft(resourcesInRepository1Map.size());

        //get all resources from connection 2
        HashMap<String, ResourceWithLexicalization> resourcesInRepository2Map =
                getResourceMap(sparqlSelectRepository2, Utils.LexicalizationType.fromValue(diffTaskStructure.getTaskInfo().getRightDataset().getLexicalizationIRI()),
        diffTaskStructure.getTaskInfo().getLangsShown());
        diffTaskStructure.getTaskExecutionInfo().setResourcesRight(resourcesInRepository2Map.size());

        diffTaskStructure.saveTaskExecutionInfo();

        //compare resources to see which have been added, removed or are present in both repositories
        compareResourcesMap(resourcesInRepository1Map, resourcesInRepository2Map);
        diffTaskStructure.saveTaskExecutionInfo();

        //regarding the resources present in both repositories, check if they are the same or if they have changed
        checkResourcesInCommon();
        diffTaskStructure.saveTaskExecutionInfo();


        //if both lexicalization types are SKOSXL, get the labels
        if(diffTaskStructure.getTaskInfo().getLeftDataset().getLexicalizationIRI().equals(Utils.LexicalizationType.SKOS_XL.getValue()) &&
                diffTaskStructure.getTaskInfo().getRightDataset().getLexicalizationIRI().equals(Utils.LexicalizationType.SKOS_XL.getValue())){

            HashMap<String, LabelWithResAndLitForm> labelsInRepository1Map =
                    getLabelsMap(sparqlSelectRepository1, Utils.LexicalizationType.fromValue(diffTaskStructure.getTaskInfo().getLeftDataset().getLexicalizationIRI()));
            diffTaskStructure.getTaskExecutionInfo().setLabelsLeft(labelsInRepository1Map.size());

            //get all resources from connection 2
            HashMap<String, LabelWithResAndLitForm> labelsInRepository2Map =
                    getLabelsMap(sparqlSelectRepository2, Utils.LexicalizationType.fromValue(diffTaskStructure.getTaskInfo().getRightDataset().getLexicalizationIRI()));
            diffTaskStructure.getTaskExecutionInfo().setLabelsRight(labelsInRepository2Map.size());

            diffTaskStructure.saveTaskExecutionInfo();

            //compare xlabel to see which have been added, removed or are the present in both repositories
            compareXlabelMap(labelsInRepository1Map, labelsInRepository2Map);
            diffTaskStructure.saveTaskExecutionInfo();

            //regarding the xlabel present in both repositories, check if they are the same or if they have changed
            checkXlabelsInCommon();
            diffTaskStructure.saveTaskExecutionInfo();
        }

        //save the result of the comparison
        DatasetInfo leftDataset = diffTaskStructure.getTaskInfo().getLeftDataset();
        DatasetInfo rightDataset = diffTaskStructure.getTaskInfo().getRightDataset();
        DiffResultStructure diffResultStructure = new DiffResultStructure(
                leftDataset.getProjectName(), leftDataset.getVersionRepoId(), leftDataset.getSparqlEndpoint(), leftDataset.getLexicalizationIRI(),
                leftDataset.getUsername(), leftDataset.getPassword(),
                rightDataset.getProjectName(), rightDataset.getVersionRepoId(), rightDataset.getSparqlEndpoint(), rightDataset.getLexicalizationIRI(),
                rightDataset.getUsername(), rightDataset.getPassword(),
                diffTaskStructure.getTaskInfo().getTaskId(), diffTaskStructure.getTaskInfo().getLangsShown(),
                removedResourceList, addedResourceList, changedResourceList,
                removedLabelList, addedLabelList, changedLabelList );

        diffTaskStructure.saveResult(diffResultStructure);
    }

    private HashMap<String, ResourceWithLexicalization> getResourceMap(SPARQLRepository sparqlSelectRepository,
                                                                       Utils.LexicalizationType lexicalizationType, List<String> langList){
        HashMap<String, ResourceWithLexicalization> resourcesToResWithLexMap = new HashMap<>();

        //first of all, do a SPARQL query to get all relevant type and their subclasses (skos:Concept, skos:ConceptScheme, skos:Collection)
        List<IRI> typeForResourcesList = new ArrayList<>();
        try(RepositoryConnection conn = sparqlSelectRepository.getConnection()) {
            String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
                    "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#> " +
                    "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
                    "\nSELECT DISTINCT ?type" +
                    "\nWHERE {"+
                    "\nVALUES ?superType { skos:Concept skos:ConceptScheme skos:Collection } "+
                    "\n?type rdfs:subClassOf* ?superType" +
                    "\n}";
            TupleQuery tupleQuery = conn.prepareTupleQuery(query);
            tupleQuery.setIncludeInferred(false);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while (tupleQueryResult.hasNext()) {
                BindingSet bindingSet = tupleQueryResult.next();
                Value type = bindingSet.getValue("type");
                if(type instanceof IRI) {
                    typeForResourcesList.add((IRI) type);
                }
            }
        }

        //if langList is null or empty, then consider the English (en) lexicalization
        if(langList == null || langList.isEmpty()){
            langList.add("en");
        }

        try(RepositoryConnection conn = sparqlSelectRepository.getConnection()) {
            String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
                    "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#> " +
                    "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>" +
                    "\nSELECT ?resource ?type ?lexicalization  " +
                    "\nWHERE { " +
                    "\nVALUES ?type {";
            //add the possible type IRI
            for(IRI type : typeForResourcesList){
                query += Utils.toNTriplesString(type)+" ";
            }
            query += "}" +
                    "\n?resource a ?type . " +
                    "\nOPTIONAL {";
            //now get the lexicalization, according to the passed langList
            if(lexicalizationType.equals(Utils.LexicalizationType.SKOS_CORE)){
                query += "\n?resource skos:prefLabel ?lexicalization .";
            } else { // lexicalization type is SKOSXL
                query +="\n?resource skosxl:prefLabel/skosxl:literalForm ?lexicalization .";
            }
            query += "\nFILTER(";
            boolean first = true;
            for(String lang : langList){
                if(first){
                    first = false;
                } else {
                    query += " || ";
                }
                query += "lang(?lexicalization) = '"+lang+"'";
            }
            query += ")" +
                    "\n}" +
                    "\n}";
            TupleQuery tupleQuery = conn.prepareTupleQuery(query);
            tupleQuery.setIncludeInferred(false);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while (tupleQueryResult.hasNext()) {
                BindingSet bindingSet = tupleQueryResult.next();
                Value resourceValue = bindingSet.getValue("resource");
                if(!(resourceValue instanceof IRI)){
                    // the resource is not an IRI, so it is a Bnode, in this case just skip it
                    continue;
                }
                String resourceString = Utils.toNTriplesString(resourceValue);
                if(!resourcesToResWithLexMap.containsKey(resourceString)){
                    ResourceWithLexicalization resourceWithLexicalization = new ResourceWithLexicalization((IRI) resourceValue);
                    resourcesToResWithLexMap.put(resourceString, resourceWithLexicalization);
                }
                ResourceWithLexicalization resourceWithLexicalization = resourcesToResWithLexMap.get(resourceString);
                IRI type = (IRI) bindingSet.getValue("type");
                resourceWithLexicalization.addResourceType(Utils.toNTriplesString(type));
                if(bindingSet.hasBinding("lexicalization")){
                    Literal lexicalization = (Literal) bindingSet.getValue("lexicalization");
                    resourceWithLexicalization.addLexicalization(lexicalization);
                }
            }
        }
        return  resourcesToResWithLexMap;
    }

    private void compareResourcesMap(HashMap<String, ResourceWithLexicalization> conceptsInRepository1Map,
            HashMap<String, ResourceWithLexicalization> conceptsInRepository2Map) {
        for(String resource : conceptsInRepository1Map.keySet()){
            if(conceptsInRepository2Map.containsKey(resource)){
                resourcesInCommonMap.put(resource, conceptsInRepository1Map.get(resource));
            } else {
                removedResourceList.add(conceptsInRepository1Map.get(resource));
            }
        }
        for(String resource : conceptsInRepository2Map.keySet()) {
            if(!conceptsInRepository1Map.containsKey(resource)){
                addedResourceList.add(conceptsInRepository2Map.get(resource));
            }
        }
        diffTaskStructure.getTaskExecutionInfo().setRemovedResources(removedResourceList.size());
        diffTaskStructure.getTaskExecutionInfo().setAddedResources(addedResourceList.size());
        diffTaskStructure.getTaskExecutionInfo().setResourcesInCommon(resourcesInCommonMap.size());
    }

    private void checkResourcesInCommon() throws IOException {
        //iterate over the common resources of the two dataset to check if there are any differences among them

        //get the lexicalization properties for the first repository and then for the second repository
        List<IRI> lexicalizationPropList1 = getLexicalizationProperty(sparqlSelectRepository1,
                Utils.LexicalizationType.fromValue(diffTaskStructure.getTaskInfo().getLeftDataset().getLexicalizationIRI()));
        List<IRI> lexicalizationPropList2 = getLexicalizationProperty(sparqlSelectRepository2,
                Utils.LexicalizationType.fromValue(diffTaskStructure.getTaskInfo().getRightDataset().getLexicalizationIRI()));

        //get the note properties for both repositories
        List<IRI> notePropList1 = getNodeProperties(sparqlSelectRepository1);
        List<IRI> notePropList2 = getNodeProperties(sparqlSelectRepository2);

        //to reduce the number of queries, take more resources with a single query
        List<String> resourceToQueryList = new ArrayList<>();
        int count = 0;
        for(String resource : resourcesInCommonMap.keySet()){
            resourceToQueryList.add(resource);
            if((++count)%NUM_RES_PER_QUERY == 0){
                compareCommonResources(resourceToQueryList, lexicalizationPropList1, lexicalizationPropList2, notePropList1, notePropList2);
                resourceToQueryList.clear();
            }
            diffTaskStructure.getTaskExecutionInfo().incrementResourcesInCommonAnalyzed();
            if(count%(NUM_RES_PER_QUERY*ITERATION_WHEN_TO_SAVE) == 0){
                diffTaskStructure.saveTaskExecutionInfo();
            }
        }
        //do the query on the last list of concepts (if such list is not empty)
        compareCommonResources(resourceToQueryList, lexicalizationPropList1, lexicalizationPropList2, notePropList1, notePropList2);
    }

    private List<IRI> getLexicalizationProperty(SPARQLRepository sparqlSelectRepository,
            Utils.LexicalizationType lexicalizationType){
        //do a SPARQL query to get all possible lexicalization properties  and their rdfs:subPropertyOf
        // (skosxl:prefLabel, skoxl:altLabel, skosxl:hiddenLabel)
        List<IRI> lexicalizationPropList = new ArrayList<>();
        if(lexicalizationType == null){
            return lexicalizationPropList;
        }
        try(RepositoryConnection conn = sparqlSelectRepository.getConnection()) {
            String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
                    "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#> " +
                    "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
                    "\nSELECT DISTINCT ?lexProp" +
                    "\nWHERE {";
            if(lexicalizationType.equals(Utils.LexicalizationType.SKOS_CORE)){
                query += "\nVALUES ?superLexProp { skos:prefLabel skos:altLabel skos:hiddenLabel }";
            } else {
                query += "\nVALUES ?superLexProp { skosxl:prefLabel skosxl:altLabel skosxl:hiddenLabel } ";
            }
            query += "\n?lexProp rdfs:subPropertyOf* ?superLexProp ." +
                    "\n}";
            TupleQuery tupleQuery = conn.prepareTupleQuery(query);
            tupleQuery.setIncludeInferred(false);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while (tupleQueryResult.hasNext()) {
                BindingSet bindingSet = tupleQueryResult.next();
                Value lexProp = bindingSet.getValue("lexProp");
                if(lexProp instanceof IRI) {
                    lexicalizationPropList.add((IRI) lexProp);
                }
            }
        }
        return lexicalizationPropList;
    }

    private List<IRI> getNodeProperties(SPARQLRepository sparqlSelectRepository){
        //do a SPARQL query to get all possible node properties (rdfs:subPropertyOf of skos:note)
        List<IRI> notePropList = new ArrayList<>();
        try(RepositoryConnection conn = sparqlSelectRepository.getConnection()) {
            String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
                    "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#> " +
                    "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
                    "\nSELECT DISTINCT ?noteProp" +
                    "\nWHERE {" +
                    "\n?noteProp rdfs:subPropertyOf* skos:note ." +
                    "\n}";
            TupleQuery tupleQuery = conn.prepareTupleQuery(query);
            tupleQuery.setIncludeInferred(false);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while (tupleQueryResult.hasNext()) {
                BindingSet bindingSet = tupleQueryResult.next();
                Value noteProp = bindingSet.getValue("noteProp");
                if(noteProp instanceof IRI) {
                    notePropList.add((IRI) noteProp);
                }
            }
        }
        return notePropList;
    }

    private void compareCommonResources(List<String> resourceList, List<IRI> lexicalizationPropList1,
            List<IRI>lexicalizationPropList2, List<IRI>notePropList1, List<IRI> notePropList2){
        if(resourceList.isEmpty()){
            //the list is empty, so do nothing and return
            return;
        }

        Map<String, ResourceWithInfo> resourceToResourceWithInfoMap1 = new HashMap<>();
        Map<String, ResourceWithInfo> resourceToResourceWithInfoMap2 = new HashMap<>();
        //query the first repository
        try(RepositoryConnection conn = sparqlSelectRepository1.getConnection()) {
            String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
                    "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#> " +
                    "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
                    "\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
                    "\nSELECT ?resource ?lexicalizationProp ?lexicalizationValue ?prop ?value ?noteProp ?noteLiteralValue " +
                    "\nWHERE {";

            //add the desired resources
            query += "\nVALUES ?resource { " ;
            for(String resource : resourceList){
                query += resource+" ";
            }
            query +="} ";

            query += "\n?resource ?prop ?value . ";
            //add the lexicalization part, according to what is the lexicalization of the repository
            query += addLexicalizationPart(lexicalizationPropList1,
                    Utils.LexicalizationType.fromValue(diffTaskStructure.getTaskInfo().getLeftDataset().getLexicalizationIRI()),
                    "?resource", "?lexicalizationProp", "?lexicalizationValue", "?xLabel");
            //add the note part (for the reified notes)
            query += "OPTIONAL {" +
                    "\nVALUES ?noteProp {";
            for(IRI notePropIri : notePropList1){
                query += Utils.toNTriplesString(notePropIri)+" ";
            }
            query += "}" +
                    "\n?resource ?noteProp ?reifiedNote ." +
                    "\n?reifiedNote rdf:value ?noteLiteralValue ." +
                    "\n}";

            query += "\n}";
            TupleQuery tupleQuery = conn.prepareTupleQuery(query);
            tupleQuery.setIncludeInferred(false);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while(tupleQueryResult.hasNext()){
                BindingSet bindingSet = tupleQueryResult.next();
                IRI resourceIri = (IRI) bindingSet.getValue("resource");
                IRI lexicalizationProp = null;
                Literal lexicalizationValue = null;
                if(bindingSet.hasBinding("lexicalizationValue")){
                    if(bindingSet.getValue("lexicalizationValue") instanceof Literal){
                        //if the lexicalizationValue is not a literal, then do not consider this Lexicalization
                        lexicalizationProp = (IRI) bindingSet.getValue("lexicalizationProp");
                        lexicalizationValue = (Literal) bindingSet.getValue("lexicalizationValue");
                    }
                }
                IRI prop = (IRI) bindingSet.getValue("prop");
                Value value = bindingSet.getValue("value");
                IRI noteProp = null;
                Literal noteLiteralValue = null;
                if(bindingSet.hasBinding("noteLiteralValue")){
                    if(bindingSet.getValue("noteLiteralValue") instanceof  Literal){
                        //if the noteLiteralValue is not a literal, then do not consider the reified note
                        noteProp = (IRI)bindingSet.getValue("noteProp");
                        noteLiteralValue = (Literal) bindingSet.getValue("noteLiteralValue");
                    }
                }
                //now construct the object for such resource or, if there is already an object for this resouce, add these information
                // to such object
                String resourceString = Utils.toNTriplesString(resourceIri);
                if(!resourceToResourceWithInfoMap1.containsKey(resourceString)){
                    resourceToResourceWithInfoMap1.put(resourceString, new ResourceWithInfo(resourceIri));
                }
                ResourceWithInfo resourceWithInfo = resourceToResourceWithInfoMap1.get(resourceString);
                resourceWithInfo.addPropValue(prop, value);
                resourceWithInfo.addLexPropLexValue(lexicalizationProp, lexicalizationValue);
                resourceWithInfo.addReifiedNote(noteProp, noteLiteralValue);
            }
        }
        //query the second repository
        try(RepositoryConnection conn = sparqlSelectRepository2.getConnection()) {
            String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
                    "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#> " +
                    "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
                    "\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
                    "\nSELECT ?resource ?lexicalizationProp ?lexicalizationValue ?prop ?value ?noteProp ?noteLiteralValue " +
                    "\nWHERE {";
            //add the desired resources
            query += "\nVALUES ?resource { " ;
            for(String resource : resourceList){
                query += resource+" ";
            }

            query +="} ";

            query += "\n?resource ?prop ?value . ";
            //add the lexicalization part, according to what is the lexicalization of the respository
            query += addLexicalizationPart(lexicalizationPropList2,
                    Utils.LexicalizationType.fromValue(diffTaskStructure.getTaskInfo().getRightDataset().getLexicalizationIRI()),
                    "?resource", "?lexicalizationProp", "?lexicalizationValue", "?xLabel") ;
            //add the note part (for the reified notes)
            query += "OPTIONAL {" +
                    "\nVALUES ?noteProp {";
            for(IRI notePropIri : notePropList2){
                query += Utils.toNTriplesString(notePropIri)+" ";
            }
            query += "}" +
                    "\n?resource ?noteProp ?reifiedNote ." +
                    "\n?reifiedNote rdf:value ?noteLiteralValue ." +
                    "\n}";

            query += "\n}";
            TupleQuery tupleQuery = conn.prepareTupleQuery(query);
            tupleQuery.setIncludeInferred(false);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while(tupleQueryResult.hasNext()){
                BindingSet bindingSet = tupleQueryResult.next();
                IRI resourceIri = (IRI) bindingSet.getValue("resource");
                IRI lexicalizationProp = null;
                Literal lexicalizationValue = null;
                if(bindingSet.hasBinding("lexicalizationProp")){
                    if(bindingSet.getValue("lexicalizationValue") instanceof Literal){
                        //if the lexicalizationValue is not a literal, then do not consider this Lexicalization
                        lexicalizationProp = (IRI) bindingSet.getValue("lexicalizationProp");
                        lexicalizationValue = (Literal) bindingSet.getValue("lexicalizationValue");
                    }
                }
                IRI prop = (IRI) bindingSet.getValue("prop");
                Value value = bindingSet.getValue("value");
                IRI noteProp = null;
                Literal noteLiteralValue = null;
                if(bindingSet.hasBinding("noteLiteralValue")){
                    if(bindingSet.getValue("noteLiteralValue") instanceof  Literal){
                        //if the noteLiteralValue is not a literal, then do not consider the reified note
                        noteProp = (IRI)bindingSet.getValue("noteProp");
                        noteLiteralValue = (Literal) bindingSet.getValue("noteLiteralValue");
                    }
                }
                //now construct the object for such resource or, if there is already an object for this resouce, add these information
                // to such object
                String resourceString = Utils.toNTriplesString(resourceIri);
                if(!resourceToResourceWithInfoMap2.containsKey(resourceString)){
                    resourceToResourceWithInfoMap2.put(resourceString, new ResourceWithInfo(resourceIri));
                }
                ResourceWithInfo resourceWithInfo = resourceToResourceWithInfoMap2.get(resourceString);
                resourceWithInfo.addPropValue(prop, value);
                resourceWithInfo.addLexPropLexValue(lexicalizationProp, lexicalizationValue);
                resourceWithInfo.addReifiedNote(noteProp, noteLiteralValue);
            }
        }

        //compare the information associated to the resources of the first dataset and to those of the second dataset
        for(String resource : resourceList){
            ResourceWithInfo resourceWithInfo1 = resourceToResourceWithInfoMap1.get(resource);
            ResourceWithInfo resourceWithInfo2 = resourceToResourceWithInfoMap2.get(resource);
            ChangedResource changedResource = compareResourceWithInfo(resourceWithInfo1, resourceWithInfo2);
            if(changedResource != null){
                //the changed resource was created, since at least one thing was changed in this resource
                //resourceTochangedResourceMap.put(resource, changedResource);
                changedResourceList.add(changedResource);
            }
        }
    }

    private ChangedResource compareResourceWithInfo(ResourceWithInfo resourceWithInfo1, ResourceWithInfo resourceWithInfo2){
        boolean addChangedResource = false;
        IRI resourceIri = resourceWithInfo1.getResourceIri();
        ChangedResource changedResource = new ChangedResource(resourceIri);

        //compare the lexicalization from resource1 to resource2 (to get the possible removed Lexicalization)
        for(String lexPropString : resourceWithInfo1.getLexPropToLexValueListMap().keySet()){
            List<Literal> lexicalizationList1 = resourceWithInfo1.getLexPropToLexValueListMap().get(lexPropString);
            List<Literal> lexicalizationList2 = resourceWithInfo2.getLexPropToLexValueListMap().get(lexPropString);
            if(lexicalizationList2 == null) {
                lexicalizationList2 = new ArrayList<>();
            }

            for(Literal lexicalization1 : lexicalizationList1){
                //check if lexicalization1 is present in lexicalizationList2
                boolean found = false;
                for(Literal lexicalization2 : lexicalizationList2){
                    if(Utils.toNTriplesString(lexicalization1).equals(Utils.toNTriplesString(lexicalization2))){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    //the lexicalization was not found, so add the lexPropString-lexicalization1 to changedResource
                    addChangedResource = true;
                    changedResource.addLexPropAndRemovedLexicalization(Utils.createIRI(lexPropString), lexicalization1);
                }
            }
        }

        //compare the lexicalization from resource2 to resource1 (to get the possible added Lexicalization)
        for(String lexPropString : resourceWithInfo2.getLexPropToLexValueListMap().keySet()){
            List<Literal> lexicalizationList2 = resourceWithInfo2.getLexPropToLexValueListMap().get(lexPropString);
            List<Literal> lexicalizationList1 = resourceWithInfo1.getLexPropToLexValueListMap().get(lexPropString);
            if(lexicalizationList1 == null) {
                lexicalizationList1 = new ArrayList<>();
            }

            for(Literal lexicalization2 : lexicalizationList2){
                //check if lexicalization1 is present in lexicalizationList2
                boolean found = false;
                for(Literal lexicalization1 : lexicalizationList1){
                    if(Utils.toNTriplesString(lexicalization2).equals(Utils.toNTriplesString(lexicalization1))){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    //the lexicalization was not found, so add the lexPropString-lexicalization1 to changedResource
                    addChangedResource = true;
                    changedResource.addLexPropAndAddedLexicalization(Utils.createIRI(lexPropString), lexicalization2);
                }
            }
        }

        //compare the reified note from resource1 to resource2 (to get the possible removed note)
        for(String notePropString : resourceWithInfo1.getNotePropToNoteValueListMap().keySet()){
            List<Literal> noteValueList1 = resourceWithInfo1.getNotePropToNoteValueListMap().get(notePropString);
            List<Literal> noteValueList2 = resourceWithInfo2.getNotePropToNoteValueListMap().get(notePropString);
            if(noteValueList2 == null){
                noteValueList2 = new ArrayList<>();
            }

            for(Literal noteValue1 : noteValueList1){
                //check if noteValue1 is present in NoteValueList2
                boolean found = false;
                for(Literal noteValue2 : noteValueList2){
                    if(Utils.toNTriplesString(noteValue1).equals(Utils.toNTriplesString(noteValue2))){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    //the noteValue was not found, so add the notePropString-noteValue1 to changedResource
                    addChangedResource = true;
                    changedResource.addNotePropAndRemovedNoteValue(Utils.createIRI(notePropString), noteValue1);
                }
            }
        }

        //compare the reified note from resource2 to resource1 (to get the possible added note)
        for(String notePropString : resourceWithInfo2.getNotePropToNoteValueListMap().keySet()){
            List<Literal> noteValueList2 = resourceWithInfo2.getNotePropToNoteValueListMap().get(notePropString);
            List<Literal> noteValueList1 = resourceWithInfo1.getNotePropToNoteValueListMap().get(notePropString);
            if(noteValueList1 == null){
                noteValueList1 = new ArrayList<>();
            }

            for(Literal noteValue2 : noteValueList2){
                //check if noteValue1 is present in NoteValueList2
                boolean found = false;
                for(Literal noteValue1 : noteValueList1){
                    if(Utils.toNTriplesString(noteValue2).equals(Utils.toNTriplesString(noteValue1))){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    //the lexicalization was not found, so add the lexPropString-lexicalization1 to changedResource
                    addChangedResource = true;
                    changedResource.addNotePropAndAddedNoteValue(Utils.createIRI(notePropString), noteValue2);
                }
            }
        }


        //compare the prop-value from resource1 to resource2 (to get the possible remove prop-value)
        for(String propString : resourceWithInfo1.getPropToValueListMap().keySet()){
            List<Value> valueList1 = resourceWithInfo1.getPropToValueListMap().get(propString);
            List<Value> valueList2 = resourceWithInfo2.getPropToValueListMap().get(propString);
            if(valueList2 == null){
                valueList2 = new ArrayList<>();
            }

            for(Value value1 : valueList1){
                //check if value is present in valueList2
                boolean found = false;
                for(Value value2 : valueList2){
                    if(Utils.toNTriplesString(value1).equals(Utils.toNTriplesString(value2))){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    //the value was not found, so add the prop-value to changedResource
                    addChangedResource = true;
                    changedResource.addPropToRemovedValue(Utils.createIRI(propString), value1);
                }
            }
        }

        //compare the prop-value from resource2 to resource1 (to get the possible added prop-value)
        for(String propString : resourceWithInfo2.getPropToValueListMap().keySet()){
            List<Value> valueList2 = resourceWithInfo2.getPropToValueListMap().get(propString);
            List<Value> valueList1 = resourceWithInfo1.getPropToValueListMap().get(propString);
            if(valueList1 == null){
                valueList1 = new ArrayList<>();
            }

            for(Value value2 : valueList2){
                //check if value is present in valueList2
                boolean found = false;
                for(Value value1 : valueList1){
                    if(Utils.toNTriplesString(value2).equals(Utils.toNTriplesString(value1))){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    //the value was not found, so add the prop-value to changedResource
                    addChangedResource = true;
                    changedResource.addPropToAddedValue(Utils.createIRI(propString), value2);
                }
            }
        }

        if(addChangedResource){
            return changedResource;
        }
        //the changed resource is empty, so just return null
        return  null;
    }

    private String addLexicalizationPart(List<IRI> lexicalizationPropList, Utils.LexicalizationType lexicalizationType,
            String varResource, String varLexProp, String varLexValue, String varXLabel){
        String queryPart = "\nOPTIONAL { "+
                "\nVALUES "+varLexProp+" { ";
        for(IRI lexicalizationProp : lexicalizationPropList){
            queryPart += Utils.toNTriplesString(lexicalizationProp) + " ";
        }
        queryPart += "} ";
        if(lexicalizationType.equals(Utils.LexicalizationType.SKOS_CORE)){
            queryPart += "\n"+varResource+" "+varLexProp+" "+varLexValue+" .";
        }else { // it is SKOSXL
            queryPart += "\n"+varResource+" "+varLexProp+" "+varXLabel+" ."+
                    "\n"+varXLabel+" skosxl:literalForm "+varLexValue+" .";
        }
        queryPart += "\n}";


        return queryPart;
    }

    private HashMap<String, LabelWithResAndLitForm> getLabelsMap(SPARQLRepository sparqlSelectRepository,
            Utils.LexicalizationType lexicalizationType){

        HashMap<String, LabelWithResAndLitForm> xlabelToLabelsWithResAndLitFormMap = new HashMap<>();

        //first of all, do a SPARQL query to get all relevant type and their subclasses (skosxl:Label)
        List<IRI> typeForLabelsList = new ArrayList<>();
        try(RepositoryConnection conn = sparqlSelectRepository.getConnection()) {
            String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
                    "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#> " +
                    "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
                    "\nSELECT DISTINCT ?type" +
                    "\nWHERE {"+
                    "\nVALUES ?superType { skosxl:Label } "+
                    "\n?type rdfs:subClassOf* ?superType" +
                    "\n}";
            TupleQuery tupleQuery = conn.prepareTupleQuery(query);
            tupleQuery.setIncludeInferred(false);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while (tupleQueryResult.hasNext()) {
                BindingSet bindingSet = tupleQueryResult.next();
                Value type = bindingSet.getValue("type");
                if(type instanceof IRI) {
                    typeForLabelsList.add((IRI) type);
                }
            }
        }

        //do a SPARQL query to get all possible lexicalization properties  and their rdfs:subPropertyOf
        // (skosxl:prefLabel, skoxl:altLabel, skosxl:hiddenLabel)
        List<IRI> lexicalizationPropList = getLexicalizationProperty(sparqlSelectRepository,
                Utils.LexicalizationType.SKOS_XL);

        //do a SPARQL query to get the xlabel, its skosxl:literalForm and the resource it is associated to
        try(RepositoryConnection conn = sparqlSelectRepository.getConnection()) {
            String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
                    "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>" +
                    "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>" +
                    "\nSELECT ?resource ?xlabel ?literalForm  " +
                    "\nWHERE { " +
                    "\nVALUES ?type {";
            //add add possible type IRI
            for(IRI type : typeForLabelsList){
                query += Utils.toNTriplesString(type)+" ";
            }
            query += "}" +
                    "\n?xlabel a ?type . "+
            //get the resource linked with this xlabel via one of the lexicalization properties
                    "\nVALUES ?lexProp {";
            //add add possible type IRI
            for(IRI lexProp : lexicalizationPropList){
                query += Utils.toNTriplesString(lexProp)+" ";
            }
            query += "}" +
                    "\n?resource ?lexProp ?xlabel ."+
                    "\n?xlabel skosxl:literalForm ?literalForm." +
                    "\n}";
            TupleQuery tupleQuery = conn.prepareTupleQuery(query);
            tupleQuery.setIncludeInferred(false);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while (tupleQueryResult.hasNext()) {
                BindingSet bindingSet = tupleQueryResult.next();
                Value xlabelValue = bindingSet.getValue("xlabel");
                if(!(xlabelValue instanceof IRI)){
                    // the xlabel is not an IRI, so it is a Bnode, in this case just skip it
                    continue;
                }
                String xlabelString = Utils.toNTriplesString(xlabelValue);
                Value resource = bindingSet.getValue("resource");
                if(!(resource instanceof IRI)){
                    continue;
                }
                Value literalForm = bindingSet.getValue("literalForm");
                if(!(literalForm instanceof Literal)){
                    continue;
                }
                LabelWithResAndLitForm labelsWithResAndLitForm = new LabelWithResAndLitForm((IRI)resource,
                        (IRI)xlabelValue, (Literal) literalForm);
                xlabelToLabelsWithResAndLitFormMap.put(xlabelString, labelsWithResAndLitForm);
            }
        }
        return  xlabelToLabelsWithResAndLitFormMap;
    }

    private void compareXlabelMap(HashMap<String, LabelWithResAndLitForm> labelsInRepository1Map,
            HashMap<String, LabelWithResAndLitForm> labelsInRepository2Map) {
        for(String xlabel : labelsInRepository1Map.keySet()){
            if(labelsInRepository2Map.containsKey(xlabel)){
                labelsInCommonMap.put(xlabel, labelsInRepository1Map.get(xlabel));
            } else {
                //removedLabelsMap.put(xlabel, labelsInRepository1Map.get(xlabel));
                removedLabelList.add(labelsInRepository1Map.get(xlabel));
            }
        }
        for(String xlabel : labelsInRepository2Map.keySet()) {
            if(!labelsInRepository1Map.containsKey(xlabel)){
                //addedLabelsMap.put(xlabel, labelsInRepository2Map.get(xlabel));
                addedLabelList.add(labelsInRepository2Map.get(xlabel));
            }
        }
        diffTaskStructure.getTaskExecutionInfo().setRemovedLabels(removedLabelList.size());
        diffTaskStructure.getTaskExecutionInfo().setAddedLabels(addedLabelList.size());
        diffTaskStructure.getTaskExecutionInfo().setLabelsInCommon(labelsInCommonMap.size());
    }

    private void checkXlabelsInCommon() throws IOException {
        //iterate over the common xlabels of the two dataset to check if there are any differences among them
        List<IRI> lexicalizationPropList1 = getLexicalizationProperty(sparqlSelectRepository1,
                Utils.LexicalizationType.fromValue(diffTaskStructure.getTaskInfo().getLeftDataset().getLexicalizationIRI()));
        List<IRI> lexicalizationPropList2 = getLexicalizationProperty(sparqlSelectRepository2,
                Utils.LexicalizationType.fromValue(diffTaskStructure.getTaskInfo().getRightDataset().getLexicalizationIRI()));
        //get the note properties for both repositories
        List<IRI> notePropList1 = getNodeProperties(sparqlSelectRepository1);
        List<IRI> notePropList2 = getNodeProperties(sparqlSelectRepository2);

        //to reduce the number of queries, take more xlabels with a single query
        List<String> xlabelsToQueryList = new ArrayList<>();
        int count = 0;
        for(String xlabel : labelsInCommonMap.keySet()){
            xlabelsToQueryList.add(xlabel);
            if((++count)%NUM_RES_PER_QUERY == 0){
                compareCommonLabels(xlabelsToQueryList, lexicalizationPropList1, lexicalizationPropList2, notePropList1, notePropList2);
                xlabelsToQueryList.clear();
            }
            diffTaskStructure.getTaskExecutionInfo().incrementLabelsInCommonAnalyzes();
            if(count%(NUM_RES_PER_QUERY*ITERATION_WHEN_TO_SAVE) == 0){
                diffTaskStructure.saveTaskExecutionInfo();
            }
        }
        //do the query on the last list of xlabels (if such list is not empty)
        compareCommonLabels(xlabelsToQueryList, lexicalizationPropList1, lexicalizationPropList2, notePropList1, notePropList2);
    }

    private void compareCommonLabels(List<String> xlabelsList, List<IRI> lexicalizationPropList1,
            List<IRI>lexicalizationPropList2, List<IRI>notePropList1, List<IRI> notePropList2) {
        if(xlabelsList.isEmpty()){
            //the list is empty, so do nothing and return
            return;
        }

        Map<String, LabelWithInfo> labelToLabelWithInfoMap1 = new HashMap<>();
        Map<String, LabelWithInfo> labelToLabelWithInfoMap2 = new HashMap<>();
        //query the first repository
        try(RepositoryConnection conn = sparqlSelectRepository1.getConnection()) {
            String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
                    "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#> " +
                    "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
                    "\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
                    "\nSELECT ?resource ?xlabel ?literalForm ?prop ?value ?noteProp ?noteLiteralValue " +
                    "\nWHERE {";

            //add the desired labels
            query += "\nVALUES ?xlabel { " ;
            for(String xlabel : xlabelsList){
                query += xlabel+" ";
            }
            query +="} " +
                    "\n?xlabel skosxl:literalForm ?literalForm .";

            //add the lexical properties
            query += "\nVALUES ?lexProp { " ;
            for(IRI lexProp : lexicalizationPropList1){
                query += Utils.toNTriplesString(lexProp)+" ";
            }
            query +="} " +
                    "\n?resource ?lexProp ?xlabel . "+
                    "\n?xlabel ?prop ?value .";
            //add the note part (for the reified notes)
            query += "OPTIONAL {" +
                    "\nVALUES ?noteProp {";
            for(IRI notePropIri : notePropList1){
                query += Utils.toNTriplesString(notePropIri)+" ";
            }
            query += "}" +
                    "\n?xlabel ?noteProp ?reifiedNote ." +
                    "\n?reifiedNote rdf:value ?noteLiteralValue ." +
                    "\n}";

            query += "\n}";

            TupleQuery tupleQuery = conn.prepareTupleQuery(query);
            tupleQuery.setIncludeInferred(false);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while(tupleQueryResult.hasNext()){
                BindingSet bindingSet = tupleQueryResult.next();
                IRI labelIri = (IRI)bindingSet.getValue("xlabel");
                Value resource = bindingSet.getValue("resource");
                if(!(resource instanceof IRI)){
                    //the resource to which this skosxl:Label is associated is not an IRI, so skipt it
                    continue;
                }
                if(!(bindingSet.getValue("literalForm") instanceof Literal)){
                    //the literalForm is not a Literal, so skip it (this should never happen in a well formed thesaurus)
                    continue;
                }
                Literal literalForm = (Literal) bindingSet.getValue("literalForm");
                IRI prop = (IRI) bindingSet.getValue("prop");
                Value value = bindingSet.getValue("value");
                String labelString = Utils.toNTriplesString(labelIri);
                IRI noteProp = null;
                Literal noteLiteralValue = null;
                if(bindingSet.hasBinding("noteLiteralValue")){
                    if(bindingSet.getValue("noteLiteralValue") instanceof  Literal){
                        //if the noteLiteralValue is not a literal, then do not consider the reified note
                        noteProp = (IRI)bindingSet.getValue("noteProp");
                        noteLiteralValue = (Literal) bindingSet.getValue("noteLiteralValue");
                    }
                }
                if(!labelToLabelWithInfoMap1.containsKey(labelString)){
                    labelToLabelWithInfoMap1.put(labelString, new LabelWithInfo(labelIri));
                }
                LabelWithInfo labelWithInfo = labelToLabelWithInfoMap1.get(labelString);
                labelWithInfo.setLiteralForm(literalForm);
                labelWithInfo.setResource((IRI) resource);
                labelWithInfo.addPropValue(prop, value);
                labelWithInfo.addReifiedNote(noteProp, noteLiteralValue);
            }
        }


        //query the second repository
        try(RepositoryConnection conn = sparqlSelectRepository2.getConnection()) {
            String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
                    "\nPREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#> " +
                    "\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
                    "\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
                    "\nSELECT ?resource ?xlabel ?literalForm ?prop ?value ?noteProp ?noteLiteralValue " +
                    "\nWHERE {";

            //add the desired labels
            query += "\nVALUES ?xlabel { " ;
            for(String label : xlabelsList){
                query += label+" ";
            }
            query +="} " +
                    "\n?xlabel skosxl:literalForm ?literalForm .";

            //add the lexical properties
            query += "\nVALUES ?lexProp { " ;
            for(IRI lexProp : lexicalizationPropList2){
                query += Utils.toNTriplesString(lexProp)+" ";
            }
            query +="} " +
                    "\n?resource ?lexProp ?xlabel . "+
                    "\n?xlabel ?prop ?value .";
            //add the note part (for the reified notes)
            query += "OPTIONAL {" +
                    "\nVALUES ?noteProp {";
            for(IRI notePropIri : notePropList2){
                query += Utils.toNTriplesString(notePropIri)+" ";
            }
            query += "}" +
                    "\n?xlabel ?noteProp ?reifiedNote ." +
                    "\n?reifiedNote rdf:value ?noteLiteralValue ." +
                    "\n}";

            query += "\n}";

            TupleQuery tupleQuery = conn.prepareTupleQuery(query);
            tupleQuery.setIncludeInferred(false);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while(tupleQueryResult.hasNext()){
                BindingSet bindingSet = tupleQueryResult.next();
                IRI labelIri = (IRI)bindingSet.getValue("xlabel");
                Value resource = bindingSet.getValue("resource");
                if(!(resource instanceof IRI)){
                    //the resource to which this skosxl:Label is associated is not an IRI, so skipt it
                    continue;
                }
                if(!(bindingSet.getValue("literalForm") instanceof Literal)){
                    //the literalForm is not a Literal, so skip it
                    continue;
                }
                Literal literalForm = (Literal) bindingSet.getValue("literalForm");
                IRI prop = (IRI) bindingSet.getValue("prop");
                Value value = bindingSet.getValue("value");
                String labelString = Utils.toNTriplesString(labelIri);
                IRI noteProp = null;
                Literal noteLiteralValue = null;
                if(bindingSet.hasBinding("noteLiteralValue")){
                    if(bindingSet.getValue("noteLiteralValue") instanceof  Literal){
                        //if the noteLiteralValue is not a literal, then do not consider the reified note
                        noteProp = (IRI)bindingSet.getValue("noteProp");
                        noteLiteralValue = (Literal) bindingSet.getValue("noteLiteralValue");
                    }
                }
                if(!labelToLabelWithInfoMap2.containsKey(labelString)){
                    labelToLabelWithInfoMap2.put(labelString, new LabelWithInfo(labelIri));
                }
                LabelWithInfo labelWithInfo = labelToLabelWithInfoMap2.get(labelString);
                labelWithInfo.setLiteralForm(literalForm);
                labelWithInfo.setResource((IRI) resource);
                labelWithInfo.addPropValue(prop, value);
                labelWithInfo.addReifiedNote(noteProp, noteLiteralValue);
            }
        }

        //compare the information associated to the label of the first dataset and to those of the second dataset
        for(String label : xlabelsList){
            LabelWithInfo labelWithInfo1 = labelToLabelWithInfoMap1.get(label);
            LabelWithInfo labelWithInfo2 = labelToLabelWithInfoMap2.get(label);
            ChangedLabel changedLabel = compareLabelWithInfo(labelWithInfo1, labelWithInfo2);
            if(changedLabel != null){
                //the changed resource was created, since at least one thing was changed in this resource
                //labelTochangedLabelMap.put(label, changedLabel);
                changedLabelList.add(changedLabel);
            }
        }
    }

    private ChangedLabel compareLabelWithInfo(LabelWithInfo labelWithInfo1, LabelWithInfo labelWithInfo2){
        boolean addChangedLabel = false;
        IRI label = labelWithInfo1.getLabel();
        ChangedLabel changedLabel = new ChangedLabel(label);

        //compare the literalForm from dataset1 to dataset2
        Literal literalForm1 = labelWithInfo1.getLiteralForm();
        Literal literalForm2 = labelWithInfo2.getLiteralForm();
        if(Utils.toNTriplesString(literalForm1).equals(Utils.toNTriplesString(literalForm2))){
            //the two literalForm are the same, so take one
            changedLabel.setLiteralForm(literalForm1);
        } else {
            //the two literalForm are different, so one was removed and the other was added
            changedLabel.setRemovedLiteralForm(literalForm1);
            changedLabel.setAddedLiteralForm(literalForm2);
            addChangedLabel = true;
        }

        //compare the resource the label is associated to
        IRI resource1 = labelWithInfo1.getResource();
        IRI resource2 = labelWithInfo2.getResource();
        if(Utils.toNTriplesString(resource1).equals(Utils.toNTriplesString(resource2))){
            //the two resources are the same, so take one
            changedLabel.setResource(resource1);
        } else {
            //the two resources are different, so one was removed and the other was added
            changedLabel.setRemovedResouce(resource1);
            changedLabel.setAddedResource(resource2);
            addChangedLabel = true;
        }

        //take the noteProp-noteValue form label1 and compare to label2 (to get the possible removed note)
        for(String notePropString : labelWithInfo1.getNotePropToNoteValueListMap().keySet()){
            List<Literal> noteValueList1 = labelWithInfo1.getNotePropToNoteValueListMap().get(notePropString);
            List<Literal> noteValueList2 = labelWithInfo2.getNotePropToNoteValueListMap().get(notePropString);
            if(noteValueList2 == null){
                noteValueList2 = new ArrayList<>();
            }

            for(Literal noteValue1 : noteValueList1){
                //check if noteValue1 is present in NoteValueList2
                boolean found = false;
                for(Literal noteValue2 : noteValueList2){
                    if(Utils.toNTriplesString(noteValue1).equals(Utils.toNTriplesString(noteValue2))){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    //the noteValue was not found, so add the notePropString-noteValue1 to changedResource
                    addChangedLabel = true;
                    changedLabel.addNotePropAndRemovedNoteValue(Utils.createIRI(notePropString), noteValue1);
                }
            }
        }

        //take the noteProp-noteValue form label2 and compare to label1 (to get the possible added note)
        for(String notePropString : labelWithInfo2.getNotePropToNoteValueListMap().keySet()){
            List<Literal> noteValueList2 = labelWithInfo2.getNotePropToNoteValueListMap().get(notePropString);
            List<Literal> noteValueList1 = labelWithInfo1.getNotePropToNoteValueListMap().get(notePropString);
            if(noteValueList1 == null){
                noteValueList1 = new ArrayList<>();
            }

            for(Literal noteValue2 : noteValueList2){
                //check if noteValue1 is present in NoteValueList2
                boolean found = false;
                for(Literal noteValue1 : noteValueList1){
                    if(Utils.toNTriplesString(noteValue2).equals(Utils.toNTriplesString(noteValue1))){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    //the lexicalization was not found, so add the lexPropString-lexicalization1 to changedResource
                    addChangedLabel = true;
                    changedLabel.addNotePropAndAddedNoteValue(Utils.createIRI(notePropString), noteValue2);
                }
            }
        }

        //compare all other prop-value

        //take the prop-value from label1 and compare to label2
        for(String propString : labelWithInfo1.getPropToValueListMap().keySet()){
            List<Value> valueList1 = labelWithInfo1.getPropToValueListMap().get(propString);
            List<Value> valueList2 = labelWithInfo2.getPropToValueListMap().get(propString);
            if(valueList2==null){
                valueList2 = new ArrayList<>();
            }
            for(Value value1 : valueList1){
                //check if value is present in valueList2
                boolean found = false;
                for(Value value2 : valueList2){
                    if(Utils.toNTriplesString(value1).equals(Utils.toNTriplesString(value2))){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    //the value was not found, so add the prop-value to changedResource
                    addChangedLabel = true;
                    changedLabel.addPropToRemovedValue(Utils.createIRI(propString), value1);
                }
            }
        }

        //take the prop-value from label2 and compare to label1
        for(String propString : labelWithInfo2.getPropToValueListMap().keySet()){
            List<Value> valueList2 = labelWithInfo2.getPropToValueListMap().get(propString);
            List<Value> valueList1 = labelWithInfo1.getPropToValueListMap().get(propString);
            if(valueList1 == null){
                valueList1 = new ArrayList<>();
            }
            for(Value value2 : valueList2){
                //check if value is present in valueList2
                boolean found = false;
                for(Value value1 : valueList1){
                    if(Utils.toNTriplesString(value2).equals(Utils.toNTriplesString(value1))){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    //the value was not found, so add the prop-value to changedResource
                    addChangedLabel = true;
                    changedLabel.addPropToAddedValue(Utils.createIRI(propString), value2);
                }
            }
        }

        if(addChangedLabel){
            return changedLabel;
        }
        return null;
    }

  
}
