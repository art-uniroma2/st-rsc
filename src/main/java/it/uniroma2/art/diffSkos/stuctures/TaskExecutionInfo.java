package it.uniroma2.art.diffSkos.stuctures;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TaskExecutionInfo {
    private int resourcesLeft;
    private int resourcesRight;
    private int addedResources;
    private int removedResources;
    private int resourcesInCommon;
    private int resourcesInCommonAnalyzed;

    private int labelsLeft;
    private int labelsRight;
    private int addedLabels;
    private int removedLabels;
    private int labelsInCommon;
    private int labelsInCommonAnalyzes;

    public TaskExecutionInfo() {
    }

    @JsonCreator
    public TaskExecutionInfo(@JsonProperty("resourcesLeft")int resourcesLeft, @JsonProperty("resourcesRight")int resourcesRight,
            @JsonProperty("addedResources")int addedResources, @JsonProperty("removedResources")int removedResources,
            @JsonProperty("resourcesInCommon")int resourcesInCommon, @JsonProperty("resourcesInCommonAnalyzed")int resourcesInCommonAnalyzed,
            @JsonProperty("labelsLeft")int labelsLeft, @JsonProperty("labelsRight")int labelsRight,
            @JsonProperty("addedLabels")int addedLabels, @JsonProperty("removedLabels")int removedLabels,
            @JsonProperty("labelsInCommon")int labelsInCommon, @JsonProperty("labelsInCommonAnalyzes")int labelsInCommonAnalyzes) {
        this.resourcesLeft = resourcesLeft;
        this.resourcesRight = resourcesRight;
        this.addedResources = addedResources;
        this.removedResources = removedResources;
        this.resourcesInCommon = resourcesInCommon;
        this.resourcesInCommonAnalyzed = resourcesInCommonAnalyzed;
        this.labelsLeft = labelsLeft;
        this.labelsRight = labelsRight;
        this.addedLabels = addedLabels;
        this.removedLabels = removedLabels;
        this.labelsInCommon = labelsInCommon;
        this.labelsInCommonAnalyzes = labelsInCommonAnalyzes;
    }

    public int getResourcesLeft() {
        return resourcesLeft;
    }

    public void setResourcesLeft(int resourcesLeft) {
        this.resourcesLeft = resourcesLeft;
    }

    public int getResourcesRight() {
        return resourcesRight;
    }

    public void setResourcesRight(int resourcesRight) {
        this.resourcesRight = resourcesRight;
    }

    public int getAddedResources() {
        return addedResources;
    }

    public void setAddedResources(int addedResources) {
        this.addedResources = addedResources;
    }

    public int getRemovedResources() {
        return removedResources;
    }

    public void setRemovedResources(int removedResources) {
        this.removedResources = removedResources;
    }

    public int getResourcesInCommon() {
        return resourcesInCommon;
    }

    public void setResourcesInCommon(int resourcesInCommon) {
        this.resourcesInCommon = resourcesInCommon;
        resourcesInCommonAnalyzed = 0;
    }

    public int getResourcesInCommonAnalyzed() {
        return resourcesInCommonAnalyzed;
    }

    public void incrementResourcesInCommonAnalyzed() {
        ++resourcesInCommonAnalyzed;
    }

    public int getLabelsLeft() {
        return labelsLeft;
    }

    public void setLabelsLeft(int labelsLeft) {
        this.labelsLeft = labelsLeft;
    }

    public int getLabelsRight() {
        return labelsRight;
    }

    public void setLabelsRight(int labelsRight) {
        this.labelsRight = labelsRight;
    }

    public int getAddedLabels() {
        return addedLabels;
    }

    public void setAddedLabels(int addedLabels) {
        this.addedLabels = addedLabels;
    }

    public int getRemovedLabels() {
        return removedLabels;
    }

    public void setRemovedLabels(int removedLabels) {
        this.removedLabels = removedLabels;
    }

    public int getLabelsInCommon() {
        return labelsInCommon;
    }

    public void setLabelsInCommon(int labelsInCommon) {
        this.labelsInCommon = labelsInCommon;
        labelsInCommonAnalyzes = 0;
    }

    public int getLabelsInCommonAnalyzes() {
        return labelsInCommonAnalyzes;
    }

    public void incrementLabelsInCommonAnalyzes() {
        ++labelsInCommonAnalyzes;
    }
}
