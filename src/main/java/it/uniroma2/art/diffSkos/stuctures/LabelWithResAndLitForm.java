package it.uniroma2.art.diffSkos.stuctures;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.uniroma2.art.utils.Utils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

public class LabelWithResAndLitForm {
    private String resource;
    private String label;
    private String literalForm;

    @JsonCreator
    public LabelWithResAndLitForm(@JsonProperty("resource")String resource, @JsonProperty("label")String label,
            @JsonProperty("literalForm")String literalForm) {
        this.resource = resource;
        this.label = label;
        this.literalForm = literalForm;
    }

    public LabelWithResAndLitForm(IRI resource, IRI label, Literal literalForm) {
        this.resource = Utils.toNTriplesString(resource);
        this.label = Utils.toNTriplesString(label);
        this.literalForm = Utils.toNTriplesString(literalForm);
    }

    public String getResource() {
        return resource;
    }

    public String getLabel() {
        return label;
    }

    public String getLiteralForm() {
        return literalForm;
    }
}
