package it.uniroma2.art.diffSkos.stuctures;

import it.uniroma2.art.utils.Utils;
import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResourceWithInfo {
    private IRI resourceIri;
    private Map<String, List<Value>> propToValueListMap = new HashMap<>(); //the prop is in <propIRI>
    private Map<String, List<Literal>> lexPropToLexValueListMap = new HashMap<>(); //the lexProp is in <lexProp>
    private Map<String, List<Literal>> notePropToNoteValueListMap = new HashMap<>(); // the noteProp is in <noteProp>

    public ResourceWithInfo(IRI resourceIri) {
        this.resourceIri = resourceIri;
    }

    public IRI getResourceIri() {
        return resourceIri;
    }

    public Map<String, List<Value>> getPropToValueListMap() {
        return propToValueListMap;
    }

    public void addPropValue(IRI prop, Value value){
        if(prop == null || value == null){
            return;
        }
        String propString = Utils.toNTriplesString(prop);
        if(value instanceof BNode){
            //do not add BNode
            return;
        }
        if(!propToValueListMap.containsKey(propString)){
            propToValueListMap.put(propString, new ArrayList<>());
        }
        //iterate to check if the value is already present
        for(Value existingValue : propToValueListMap.get(propString)){
            if(Utils.toNTriplesString(existingValue).equals(Utils.toNTriplesString(value))){
                //the value is already present, so just return
                return;
            }
        }
        propToValueListMap.get(propString).add(value);
    }

    public Map<String, List<Literal>> getLexPropToLexValueListMap() {
        return lexPropToLexValueListMap;
    }

    public void addLexPropLexValue(IRI lexProp, Literal lexValue){
        if(lexProp==null || lexValue == null){
            return;
        }
        String lexPropString = Utils.toNTriplesString(lexProp);
        if(!lexPropToLexValueListMap.containsKey(lexPropString)){
            lexPropToLexValueListMap.put(lexPropString, new ArrayList<>());
        }
        //iterate to check if the lexicalization is already present
        for(Literal existingLex : lexPropToLexValueListMap.get(lexPropString)){
            if(Utils.toNTriplesString(existingLex).equals(Utils.toNTriplesString(lexValue))){
                //the lexicalization is already present, so just return
                return;
            }
        }
        lexPropToLexValueListMap.get(lexPropString).add(lexValue);
    }

    public Map<String, List<Literal>> getNotePropToNoteValueListMap() {
        return notePropToNoteValueListMap;
    }

    public void addReifiedNote(IRI noteProp, Literal noteValue){
        if(noteProp==null || noteValue == null){
            return;
        }
        String notePropString = Utils.toNTriplesString(noteProp);
        if(!notePropToNoteValueListMap.containsKey(notePropString)){
            notePropToNoteValueListMap.put(notePropString, new ArrayList<>());
        }
        //iterate to check if the note is already present
        for(Literal existingNote : notePropToNoteValueListMap.get(notePropString)){
            if(Utils.toNTriplesString(existingNote).equals(Utils.toNTriplesString(noteValue))){
                //the note is already present, so just return
                return;
            }
        }
        notePropToNoteValueListMap.get(notePropString).add(noteValue);
    }
}
