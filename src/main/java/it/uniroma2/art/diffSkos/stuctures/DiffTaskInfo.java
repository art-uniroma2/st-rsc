package it.uniroma2.art.diffSkos.stuctures;

import java.util.List;

public class DiffTaskInfo {
    private DatasetInfo leftDataset;
    private DatasetInfo rightDataset;
    private String taskId;
    private String executionTime;
    private String status;
    private List<String> langsShown;

    public DiffTaskInfo() {
    }

    public DiffTaskInfo(DatasetInfo leftDataset, DatasetInfo rightDataset, String taskId, String executionTime, String status, List<String> langsShown) {
        this.leftDataset = leftDataset;
        this.rightDataset = rightDataset;
        this.taskId = taskId;
        this.executionTime = executionTime;
        this.status = status;
        this.langsShown = langsShown;
    }

    public DatasetInfo getLeftDataset() {
        return leftDataset;
    }

    public DatasetInfo getRightDataset() {
        return rightDataset;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getExecutionTime() {
        return executionTime;
    }

    public String getStatus() {
        return status;
    }

    public List<String> getLangsShown() {
        return langsShown;
    }

    public void setExecutionTime(String executionTime) {
        this.executionTime = executionTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
