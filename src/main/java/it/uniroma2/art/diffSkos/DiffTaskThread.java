package it.uniroma2.art.diffSkos;

import it.uniroma2.art.utils.Utils;

import java.io.IOException;

public class DiffTaskThread extends Thread {

    private DiffTaskStructure diffTaskStructure;


    public DiffTaskThread(DiffTaskStructure diffTaskStructure) {
        this.diffTaskStructure = diffTaskStructure;
    }

    public void run(){
        try {
            SKOSDiffTask skosDiffTask = new SKOSDiffTask(diffTaskStructure);
            long start = Utils.getTime();
            skosDiffTask.startDiffTask();
            long end = Utils.getTime();
            try {
                diffTaskStructure.updateStatus(Utils.TaskStatus.completed);
                String prettyTime = Utils.prettyPrintTime(end-start);
                diffTaskStructure.updateExecutionTime(prettyTime);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e ) {
            e.printStackTrace();
            try {
                diffTaskStructure.updateStatus(Utils.TaskStatus.error);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}
