package it.uniroma2.art.align;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import io.swagger.model.AlignmentPlan;
import io.swagger.model.ScenarioDefinition;
import io.swagger.model.Task;
import it.uniroma2.art.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static it.uniroma2.art.align.AlignTaskStructure.SCENARIO_DEFINIZION_JSON_FILE_NAME;
import static it.uniroma2.art.align.AlignTaskStructure.TASK_JSON_FILE_NAME;

public class AlignTaskManager {

    private static AlignTaskManager alignTaskManager = null;

    private final Map<String, AlignTaskStructure> idToMatchTaskStructureMap = new HashMap<>();
    private final File maindDir;

    private AlignTaskManager(File mainDir) throws IOException {
        this.maindDir = mainDir;
        if(!mainDir.exists()){
            mainDir.mkdirs();
        }
        
        //from the maindDir get all the directory and each directory name represents a previously executed diffTask, so read from it the configuration parameters
        getPrevioslyMatchTasks();
    }

    public static AlignTaskManager getInstance(File mainDir) throws IOException {
        if(alignTaskManager == null) {
            alignTaskManager = new AlignTaskManager(mainDir);
        } return alignTaskManager;
    }

    private void getPrevioslyMatchTasks() throws IOException {
        for(File taskDir : maindDir.listFiles()){
            if(!taskDir.isDirectory()){
                //it is not a directory, so skip it
                continue;
            }
            //search for the file TASK_JSON_FILE_NAME
            File taskFile = new File(taskDir, TASK_JSON_FILE_NAME);
            if (!taskFile.exists()){
                //there is no task file in this taskDir, so skip it
                continue;
            }
            File scenarioFile = new File(taskDir, SCENARIO_DEFINIZION_JSON_FILE_NAME);
            if (!scenarioFile.exists()) {
                // there is no scenario definition in this taskDir, so skip it
                continue;
            }
            //read the configuration file, written in json, and create the MatchTaskStructure

            ObjectMapper objectMapper = new ObjectMapper();
            Task task = null;
            task = objectMapper.readValue(taskFile, Task.class);
            ScenarioDefinition scenarioDefinition = objectMapper.readValue(scenarioFile, ScenarioDefinition.class);

            AlignTaskStructure diffTaskStructure = new AlignTaskStructure(task, scenarioDefinition, maindDir);
            //if any taskInfo has a value "execution" for status, set it to "error"
            if(!diffTaskStructure.getTaskInfo().getStatus().equals(Task.StatusEnum.COMPLETED)){
                diffTaskStructure.updateStatus(Task.StatusEnum.FAILED, true);
            }
            idToMatchTaskStructureMap.put(task.getId(), diffTaskStructure);
        }
    }

    public Task createDiffStructAndStartTask(AlignmentPlan alignmentPlan) throws IOException {

        //get the the sparql endpoints of the three dataset, which are used to generate the randId
        String sparqlEndpointL1 = alignmentPlan.getScenarioDefinition().getLeftDataset().getSparqlEndpoint().getEndpointURL();
        String sparqlEndpointL2 = alignmentPlan.getScenarioDefinition().getRightDataset().getSparqlEndpoint().getEndpointURL();

        //prepare the randId, which is used to identify this task
        String randId;
        randId = Utils.getInstance().generateRandId(sparqlEndpointL1, sparqlEndpointL2,
                Lists.newArrayList(idToMatchTaskStructureMap.keySet()));

        // pass the NeededForMatch and start the task
        ScenarioDefinition scenarioDefinition = alignmentPlan.getScenarioDefinition();
        AlignTaskStructure alignTaskStructure = new AlignTaskStructure(scenarioDefinition, randId, "", maindDir);
        Task task = alignTaskStructure.startMatchTask();

        String taskId = task.getId();
        alignTaskStructure.saveScenarioInfo(taskId);

        // add the newly created matchTaskStructure to the idToMatchTaskStructureMap
        idToMatchTaskStructureMap.put(taskId, alignTaskStructure);

        //return the task
        return task;
    }

    public Map<String, AlignTaskStructure> getIdToMatchTaskStructureMap(){
        return idToMatchTaskStructureMap;
    }

    public AlignTaskStructure getMatchStructFromTaskId(String taskId){
        return idToMatchTaskStructureMap.get(taskId);
    }

    public Set<String> getAllTasksIdSet() {
        return idToMatchTaskStructureMap.keySet();
    }


}
