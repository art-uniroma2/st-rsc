package it.uniroma2.art.align;

import io.swagger.model.Task;

import java.io.IOException;

public class AlignTaskThread extends Thread {

    private AlignTaskStructure alignTaskStructure;


    public AlignTaskThread(AlignTaskStructure alignTaskStructure) {
        this.alignTaskStructure = alignTaskStructure;
    }

    public void run(){
        try {
            AlignTask alignTask = new AlignTask(alignTaskStructure);
            alignTask.startMatchTask();
            try {
                alignTaskStructure.updateStatus(Task.StatusEnum.COMPLETED, false);
                alignTaskStructure.updateProgress(100, false);
                alignTaskStructure.setEndTime(AlignTaskStructure.getDateNow(), true);
            } catch (IOException e) {
                alignTaskStructure.updateStatus(Task.StatusEnum.FAILED, true);
                //TODO decited what to do
            }
        } catch (Exception e ) {
            //TODO decited what to do
            try {
                alignTaskStructure.updateStatus(Task.StatusEnum.FAILED, true);
            } catch (IOException ioException) {
                //TODO decited what to do
            }
        }
    }
}
