package it.uniroma2.art.align;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.ScenarioDefinition;
import io.swagger.model.Task;
import it.uniroma2.art.align.structures.AlignResultStructure;
import it.uniroma2.art.align.structures.Cell;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class AlignTaskStructure {

    //public static String CONFIG_FILE_NAME = "task.properties";
    public static String TASK_JSON_FILE_NAME = "task.json";
    public static String SCENARIO_DEFINIZION_JSON_FILE_NAME = "scenario.json";
    public static String RESULT_FILE_NAME = "result.rdf";
    public static String RESULT_TEMP_FILE_NAME = "result_temp.rdf";
    public static String TASK_EXEC_FILE_NAME = "taskExecution.json";


    public static final String DATETIME_PATTERN_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss";

    //private MatchTaskInfo matchTaskInfo;
    private final Task task;
    private final ScenarioDefinition scenarioDefinition;

    private File mainDir;

    public AlignTaskStructure(Task task, ScenarioDefinition scenarioDefinition, File mainDir) {
        this.task = task;
        this.scenarioDefinition = scenarioDefinition;
        this.mainDir = mainDir;
    }

    public AlignTaskStructure(ScenarioDefinition scenarioDefinition, String taskId, String exectutionTime, File mainDir) {
        this.mainDir = mainDir;
        this.scenarioDefinition = scenarioDefinition;
        // create the Task from the ScenarioDefinition
        task = new Task();
        task.setId(taskId);
        task.setLeftDataset(scenarioDefinition.getLeftDataset().getAtId());
        task.setRightDataset(scenarioDefinition.getRightDataset().getAtId());
        task.setStatus(Task.StatusEnum.SUBMITTED);
        task.setProgress(0);
        task.setSubmissionTime(getDateNow());
    }

    public Task getTaskInfo() {
        return task;
    }

    public ScenarioDefinition getScenarioDefinition() {
        return scenarioDefinition;
    }

    public File getMainDir() {
        return mainDir;
    }


    public Task startMatchTask() throws IOException {
        task.setStatus(Task.StatusEnum.RUNNING);
        File taskDir = new File(mainDir, task.getId());
        taskDir.mkdirs();
        saveTaskInfo();
        //addConfigFile();
        AlignTaskThread alignTaskThread = new AlignTaskThread(this);
        alignTaskThread.start();

        return task;
    }

    public void updateStatus(Task.StatusEnum newStatus, boolean save) throws IOException {
        task.setStatus(newStatus);
        if (save) {
            saveTaskInfo();
        }
    }

    public void updateProgress(int progress, boolean save) throws IOException {
        task.setProgress(progress);
        if (save) {
            saveTaskInfo();
        }
    }

    public void setStartTime(String time, boolean save) throws IOException {
        task.setStartTime(time);
        if (save) {
            saveTaskInfo();
        }
    }

    public void setEndTime(String time, boolean save) throws IOException {
        task.setEndTime(time);
        if (save) {
            saveTaskInfo();
        }
    }

    private File getTaskFile(){
        File taskDir = new File(mainDir, task.getId());
        File configFile = new File(taskDir, TASK_JSON_FILE_NAME);
        return configFile;
    }

    public void saveTaskInfo() throws IOException {
        ObjectMapper obj = new ObjectMapper();
        String jsonString = obj.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        File taskDir = new File(mainDir, task.getId());
        File outputFile = new File(taskDir, TASK_JSON_FILE_NAME);


        try (OutputStream os = Files.newOutputStream(outputFile.toPath());
                OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
                BufferedWriter bf = new BufferedWriter(osw)){
            bf.write(jsonString);
            bf.flush();
        }
    }

    public void saveScenarioInfo(String taskId) throws IOException {
        ObjectMapper obj = new ObjectMapper();
        String jsonString = obj.writerWithDefaultPrettyPrinter().writeValueAsString(scenarioDefinition);
        File taskDir = new File(mainDir, taskId);
        File outputFile = new File(taskDir, SCENARIO_DEFINIZION_JSON_FILE_NAME);

        try (OutputStream os = Files.newOutputStream(outputFile.toPath());
             OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
             BufferedWriter bf = new BufferedWriter(osw)){
            bf.write(jsonString);
            bf.flush();
        }
    }

    public void saveResult(AlignResultStructure alignResultStructure) throws IOException, ParserConfigurationException, TransformerException {

        // prepare the prefixes which are placed on top of the xml file
        Map<String, String> prefixToNamespaceMap = new HashMap<>();
        prefixToNamespaceMap.put("xmlns", "http://knowledgeweb.semanticweb.org/heterogeneity/alignment#");
        prefixToNamespaceMap.put("xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        prefixToNamespaceMap.put("xmlns:xsd", "http://www.w3.org/2001/XMLSchema#");
        prefixToNamespaceMap.put("xmlns:align", "http://knowledgeweb.semanticweb.org/heterogeneity/alignment#");

        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();

        // create the root element
        Element rootElem = document.createElement("rdf:RDF");
        for(String prefix : prefixToNamespaceMap.keySet()){
            rootElem.setAttribute(prefix, prefixToNamespaceMap.get(prefix));
        }
        document.appendChild(rootElem);

        // create Alignment
        Element alignmentElem = document.createElement("Alignment");
        rootElem.appendChild(alignmentElem);

        // create xml
        Element xmlElem = document.createElement("xml");
        xmlElem.setTextContent("yes");
        alignmentElem.appendChild(xmlElem);

        // create level
        Element levelElem = document.createElement("level");
        levelElem.setTextContent("0");
        alignmentElem.appendChild(levelElem);

        // create type
        Element typeElem = document.createElement("type");
        typeElem.setTextContent("??");
        alignmentElem.appendChild(typeElem);

        // create onto1
        Element onto1Elem = document.createElement("onto1");
        onto1Elem.setTextContent(alignResultStructure.getTask().getLeftDataset());
        alignmentElem.appendChild(onto1Elem);

        // create onto2
        Element onto2Elem = document.createElement("onto2");
        onto2Elem.setTextContent(alignResultStructure.getTask().getRightDataset());
        alignmentElem.appendChild(onto2Elem);

        // create uri1
        Element uri1Elem = document.createElement("uri1");
        uri1Elem.setTextContent(alignResultStructure.getScenarioDefinition().getLeftDataset().getSparqlEndpoint().getEndpointURL());
        alignmentElem.appendChild(uri1Elem);

        // create onto2
        Element uri2Elem = document.createElement("uri2");
        uri2Elem.setTextContent(alignResultStructure.getScenarioDefinition().getRightDataset().getSparqlEndpoint().getEndpointURL());
        alignmentElem.appendChild(uri2Elem);

        // add the mapping from getMatchCellList
        for(Cell cell : alignResultStructure.getAlignCellList()) {
            Element mapElem = document.createElement("map");
            alignmentElem.appendChild(mapElem);

            Element cellElem = document.createElement("Cell");
            mapElem.appendChild(cellElem);

            Element entity1Elem = document.createElement("entity1");
            entity1Elem.setAttribute("rdf:resource", cell.getEntity1().stringValue());
            cellElem.appendChild(entity1Elem);

            Element entity2Elem = document.createElement("entity2");
            entity2Elem.setAttribute("rdf:resource", cell.getEntity2().stringValue());
            cellElem.appendChild(entity2Elem);

            Element measureElem = document.createElement("measure");
            measureElem.setAttribute("rdf:datatype", "http://www.w3.org/2001/XMLSchema#float");
            measureElem.setTextContent(String.valueOf(cell.getMeasure()));
            cellElem.appendChild(measureElem);

            Element relationElem = document.createElement("relation");
            relationElem.setTextContent(cell.getRelation());
            cellElem.appendChild(relationElem);

            Element mappingPropertyElem = document.createElement("mappingProperty");
            mappingPropertyElem.setAttribute("rdf:resource", cell.getMappingProperty().stringValue());
            cellElem.appendChild(mappingPropertyElem);
        }

        // save the file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        DOMSource domSource = new DOMSource(document);
        File taskDir = new File(mainDir, alignResultStructure.getTask().getId());
        File xmlFile = new File(taskDir, RESULT_FILE_NAME);


        File tempFile = new File(taskDir, RESULT_TEMP_FILE_NAME);
        StreamResult streamResult = new StreamResult(tempFile);

        transformer.transform(domSource, streamResult);

        Files.move(Paths.get(tempFile.getPath()), Paths.get(xmlFile.getPath()));
    }


    public static String getDateNow() {
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(DATETIME_PATTERN_ISO_8601);
        String formattedTime = outputFormatter.format(LocalDateTime.now());
        formattedTime += ".000Z";
        return formattedTime;
    }
}
