package it.uniroma2.art.align;

import io.swagger.model.Alignment;
import io.swagger.model.AlignmentChain;
import io.swagger.model.DataService;
import io.swagger.model.Dataset;
import io.swagger.model.ScenarioDefinition;
import it.uniroma2.art.align.structures.Cell;
import it.uniroma2.art.align.structures.AlignResultStructure;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AlignTask {

    private final AlignTaskStructure alignTaskStructure;

    // the maps containing the different matches A-B and B-C (used then to produce the A-C)

    private final Map<String, List<String>> aToBExactMatchMap = new HashMap<>();
    private final Map<String, List<String>> aToBCloseMatch = new HashMap<>();
    private final Map<String, List<String>> aToBBroadMatchMap = new HashMap<>();
    private final Map<String, List<String>> aToBNarrowMatch = new HashMap<>();

    private final Map<String, List<String>> bToCExactMatchMap = new HashMap<>();
    private final Map<String, List<String>> bToCCloseMatch = new HashMap<>();
    private final Map<String, List<String>> bToCBroadMatchMap = new HashMap<>();
    private final Map<String, List<String>> bToCNarrowMatch = new HashMap<>();

    // the maps containing the new matching A-C (from A-B and B-C)
    private final Map<String, List<String>> aToCExactMatchMap = new HashMap<>();
    private final Map<String, List<String>> aToCCloseMatch = new HashMap<>();
    private final Map<String, List<String>> aToCBroadMatchMap = new HashMap<>();
    private final Map<String, List<String>> aToCNarrowMatch = new HashMap<>();


    // the relation values according the the EDOAL standard
    public static final String EXACT_MATCH_RELATION = "=";
    public static final String CLOSE_MATCH_RELATION = "=";
    public static final String NARROR_MATCH_RELATION = ">";
    public static final String BROAD_MATCH_RELATION = "<";


    public AlignTask(AlignTaskStructure alignTaskStructure) {
        this.alignTaskStructure = alignTaskStructure;
    }

    public void startMatchTask() throws IOException, ParserConfigurationException, TransformerException {

        // set the start time
        alignTaskStructure.setStartTime(AlignTaskStructure.getDateNow(), true);

        // process the dataset/linkset from the scenarioDefinition
        ScenarioDefinition scenarioDefinition = alignTaskStructure.getScenarioDefinition();

        // take the id of the leftDataset and the rightDataset (which will be the A and B dataset)

        // all info about the right dataset
        Dataset leftDataset = scenarioDefinition.getLeftDataset();
        //String leftSparqlEndpoint = leftDataset.getSparqlEndpoint().getEndpointURL();
        //String leftUsername = leftDataset.getSparqlEndpoint().getUsername();
        //String leftPassword = leftDataset.getSparqlEndpoint().getPassword();
        //String leftNamespace = leftDataset.getUriSpace();
        //String leftDatasetId = leftDataset.getId();

        // all the info about the left dataset
        Dataset rightDataset = scenarioDefinition.getRightDataset();
        //String rightSparqlEndpoint = rightDataset.getSparqlEndpoint().getEndpointURL();
        //String rightUsername = rightDataset.getSparqlEndpoint().getUsername();
        //String rightPassword = rightDataset.getSparqlEndpoint().getPassword();
        //String rightNamespace = rightDataset.getUriSpace();
        //String rightDatasetId = rightDataset.getId();

        Map<String, Dataset> idToSupportDatasetMap = new HashMap<>();

        // add the leftDataset and rightDataset to idToSupportDatasetMap
        idToSupportDatasetMap.put(leftDataset.getAtId(), leftDataset);
        idToSupportDatasetMap.put(rightDataset.getAtId(), rightDataset);

        // get all the info about the supportDatasets and put in a map
        List<Dataset> supportDatasetList = scenarioDefinition.getSupportDatasets();
        for (Dataset dataset : supportDatasetList){
            String datasetId = dataset.getAtId();
            idToSupportDatasetMap.put(datasetId, dataset);
        }

        // get the chains from the alignmentsPairings and for each chain (which should be composed of just two element)
        // check tht:
        // - in the first element there is the dataset A and a dataset C (be aware if the alignment s A-C or C-A)
        // - in the second element there is the dataset B and a dataset C (be aware if the alignment s B-C or C-B)
        // - in both aliments there is the same intermediate dataset C

        List<AlignmentChain> alignmentChainList = scenarioDefinition.getAlignmentChains();
        for (AlignmentChain alignmentChain : alignmentChainList) {
            // if processed is true, then the alignmentChain was used to generate the mapping, if false, there was a
            // problem so no data was extracted
            boolean processed = processChain(alignmentChain, idToSupportDatasetMap,
                    leftDataset, rightDataset);
        }

        // process the matching maps
        processMatchingMaps();

        // create the structure which will be saved
        AlignResultStructure alignResultStructure =  createResultStruct();

        // save the results in the file
        alignTaskStructure.saveResult(alignResultStructure);

    }

    private boolean processChain(AlignmentChain alignmentChain, Map<String, Dataset> idToSupportDatasetMap,
                                 Dataset leftDataset, Dataset rightDataset) {


        // try process the AlignmentChain (it is possible to have different middle Dataset, the one called C,
        // as long as the other requires are meet):
        // leftDataset is A
        // rightDataset is C
        // middleDataset is B

        List<String> chainList = alignmentChain.getChain();
        if (chainList.size()!=2) {
            // consider only the chains of two elements
            // TODO decide what to do
            return false;
        }
        // get the two elements of the chain
        String firstElemChainId = chainList.get(0);
        String secondElemChainId = chainList.get(1);

        boolean firstReverse; // true for A-B, false for B-A
        boolean secondReverse; // true for B-C, false for C-B

        String middleDatasetId;
        Dataset middleDataset;


        // get the dataset for the first element of the chain, check if they are A-B or B-A
        Dataset firstDataset = idToSupportDatasetMap.get(firstElemChainId);
        if (firstDataset==null) {
            // the id of the first element does not correspond to any dataset in the supportDatasets
            // TODO decide what to do
            return false;
        }
        if (!(firstDataset instanceof Alignment)) {
            // the first dataset is not an alignment
            // TODO decide what to do
            return false;
        }


        Alignment firstAlignment = (Alignment) firstDataset;
        String subjectTargetFirst = firstAlignment.getSubjectsTarget();
        String objectTargetFirst = firstAlignment.getObjectsTarget();

        // check whether the leftDatasetId is the subjectTarget or the objectTarget
        if (leftDataset.getAtId().equals(subjectTargetFirst)) {
            firstReverse = false;
            middleDatasetId = objectTargetFirst;
        } else if (leftDataset.getAtId().equals(objectTargetFirst)){
            firstReverse = true;
            middleDatasetId = subjectTargetFirst;
        } else {
            // the first element of the chain does not contain the leftDatasetId
            // TODO decide what to do
            return false;
        }

        // get the middleDataset
        middleDataset = idToSupportDatasetMap.get(middleDatasetId);
        if (middleDataset == null) {
            // the id of the middle dataset, from the element of the alignment, does not correspond to a dataset in supportDatasets
            // TODO decide what to do
            return false;
        }

        // get the dataset for the second element of the chain, check if they are B-C or B-C
        Dataset secondDataset = idToSupportDatasetMap.get(secondElemChainId);
        if (secondDataset==null) {
            // the id of the second element does not correspond to any dataset in the supportDatasets
            // TODO decide what to do
            return false;
        }
        if (!(secondDataset instanceof Alignment)) {
            // the second dataset is not an alignment
            // TODO decide what to do
            return false;
        }

        Alignment secondAlignment = (Alignment) secondDataset;
        String subjectTargetSecond = secondAlignment.getSubjectsTarget();
        String objectTargetSecond = secondAlignment.getObjectsTarget();
        // check whether the leftDatasetId is the subjectTarget or the objectTarget
        if (rightDataset.getAtId().equals(subjectTargetSecond)) {
            secondReverse = true;
            // check that the objectTargetFirst is the same middleDatasetId as seen in the first element of the chain
            if (!middleDatasetId.equals(objectTargetSecond)) {
                // a different dataset from the first chain is used
                // TODO decide what to do
                return false;
            }
        } else if (rightDataset.getAtId().equals(objectTargetSecond)){
            secondReverse = false;
            // check that the subjectTargetSecond is the same middleDatasetId as seen in the first element of the chain
            if (!middleDatasetId.equals(subjectTargetSecond)) {
                // a different dataset from the first chain is used
                // TODO decide what to do
                return false;
            }
        } else {
            // the first element of the chain does not contain the leftDatasetId
            // TODO decide what to do
            return false;
        }

        // process the chain of the two elements, since everything seems ok
        String subjectNamespaceFirst, objectNamespaceFirst;
        if (firstReverse) {
            // so B-A
            subjectNamespaceFirst = middleDataset.getUriSpace();
            objectNamespaceFirst = leftDataset.getUriSpace();
        } else {
            // so A-B
            subjectNamespaceFirst = leftDataset.getUriSpace();
            objectNamespaceFirst = middleDataset.getUriSpace();
        }

        String subjectNamespaceSecond, objectNamespaceSecond;
        if (secondReverse) {
            // so C-B
            subjectNamespaceSecond = rightDataset.getUriSpace();
            objectNamespaceSecond = middleDataset.getUriSpace();
        } else {
            // so B-C
            subjectNamespaceSecond = middleDataset.getUriSpace();
            objectNamespaceSecond = rightDataset.getUriSpace();
        }

        // process the first element
        DataService dataService = firstAlignment.getSparqlEndpoint();
        SPARQLRepository sparqlRepositoryFirst = new SPARQLRepository(dataService.getEndpointURL());
        if (dataService.getUsername() != null && dataService.getPassword() != null ) {
            sparqlRepositoryFirst.setUsernameAndPassword(dataService.getUsername(), dataService.getPassword());
        }
        sparqlRepositoryFirst.initialize();
        // for A-B maps
        executeQueryToFillMatchMap(sparqlRepositoryFirst, subjectNamespaceFirst, objectNamespaceFirst,
                aToBExactMatchMap, aToBCloseMatch, aToBBroadMatchMap, aToBNarrowMatch, firstReverse);

        // process the second element
        dataService = secondAlignment.getSparqlEndpoint();
        SPARQLRepository sparqlRepositorySecond = new SPARQLRepository(dataService.getEndpointURL());
        if (dataService.getUsername() != null && dataService.getPassword() != null ) {
            sparqlRepositorySecond.setUsernameAndPassword(dataService.getUsername(), dataService.getPassword());
        }
        sparqlRepositorySecond.initialize();
        // for B-C maps
        executeQueryToFillMatchMap(sparqlRepositorySecond, subjectNamespaceSecond, objectNamespaceSecond,
                bToCExactMatchMap, bToCCloseMatch, bToCBroadMatchMap, bToCNarrowMatch, secondReverse);


        return true;
    }

    private void executeQueryToFillMatchMap(SPARQLRepository sparqlSelectRepository, String namespaceSubj, String namespaceObj,
                                            Map<String, List<String>> exactMatchMap, Map<String, List<String>> closeMatchMap,
                                            Map<String, List<String>> broadMatchMap, Map<String, List<String>> narrowMatchMap,
                                            boolean reverse) {
        // execute a SPARQL query to get the mappings (skos:exactMatch, skos:closeMatch, skos:broadMatch and skos:narrowMatch)
        try (RepositoryConnection connA = sparqlSelectRepository.getConnection()){
           String query = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#>" +
                   "\nCONSTRUCT {?subj ?pred ?obj }" +
                   "\nWHERE{" +
                   "\n?subj a skos:Concept ." +
                   "\nFILTER isIRI(?subj)" +
                   "\n?subj ?pred ?obj ." +
                   "\nFILTER isIRI(?obj)" +
                   "\nFILTER(?pred = skos:exactMatch || ?pred = skos:closeMatch || ?pred = skos:broadMatch || ?pred = skos:narrowMatch )";
                   // filter using the namespace
            if (namespaceSubj!=null && !namespaceSubj.isEmpty()) {
                query+="\nFILTER REGEX(str(?subj), \"^"+namespaceSubj+"\")";
            }
            if (namespaceObj!=null && !namespaceObj.isEmpty()) {
                query+="\nFILTER REGEX(str(?obj), \"^"+namespaceObj+"\")";
            }
            query+="\n}";

            GraphQuery graphQuery = connA.prepareGraphQuery(query);
            graphQuery.setIncludeInferred(false);
            try(GraphQueryResult graphQueryResult = graphQuery.evaluate()){
                while (graphQueryResult.hasNext()) {
                    Statement statement = graphQueryResult.next();
                    // if reverse is true, then exchange the subject with the object (for the maps)
                    IRI subj = reverse ? (IRI) statement.getObject() : (IRI) statement.getSubject();
                    IRI pred = statement.getPredicate();
                    IRI obj = reverse ? (IRI) statement.getSubject() : (IRI) statement.getObject();
                    // depending on the value of pred, use the right map to save the result of the query
                    if (pred.equals(SKOS.EXACT_MATCH)) {
                        addToMap(exactMatchMap, subj.stringValue(), obj.stringValue());
                    } else if (pred.equals(SKOS.CLOSE_MATCH)) {
                        addToMap(closeMatchMap, subj.stringValue(), obj.stringValue());
                    } else if (pred.equals(SKOS.BROAD_MATCH)) {
                        addToMap(broadMatchMap, subj.stringValue(), obj.stringValue());
                    } else if (pred.equals(SKOS.NARROW_MATCH)) {
                        addToMap(narrowMatchMap, subj.stringValue(), obj.stringValue());
                    }
                }
            }
       }
    }

    private void addToMap(Map<String, List<String>> mapToAdd, String subj, String obj) {
        if (!mapToAdd.containsKey(subj)){
            mapToAdd.put(subj, new ArrayList<>());
        }
        if (!mapToAdd.get(subj).contains(obj)){
            mapToAdd.get(subj).add(obj);
        }
    }

    private void processMatchingMaps() throws IOException {
        // process the matching maps following the sequences:
        // - A exactMatch B , B exactMatch C -> A exactMatch C
        // - (A exactMatch B, B closeMatch C ) | (A closeMatch B , B exactMatch C) -> A closeMatch C
        // - A exactMatch B, B broadMatch/narrowMatch C -> A broadMatch/narrowMatch C
        // - A broadMatch/narrowMatch B, B exactMatch C -> A broadMatch/narrowMatch C
        // - A broadMatch B, B broadMatch C -> A broadMatch C
        // - A narrowMatch B, B narrowMatch C -> A narrowMatch C

        // process A exactMatch B
        for (String resA : aToBExactMatchMap.keySet()){
            // search for the following:
            // B exactMatch C -> A exactMatch C
            // B closeMatch C -> A closeMatch C
            // B broadMatch/narrowMatch C -> A broadMatch/narrowMatch C
            for (String resB : aToBExactMatchMap.get(resA)) {
                // B exactMatch C -> A exactMatch C
                addResToMap(bToCExactMatchMap, aToCExactMatchMap, resA, resB);

                // B closeMatch C -> A closeMatch C
                addResToMap(bToCCloseMatch, aToCCloseMatch, resA, resB);

                // B broadMatch C -> A broadMatch C
                addResToMap(bToCBroadMatchMap, aToCBroadMatchMap, resA, resB);

                // B narrowMatch C -> A narrowMatch C
                addResToMap(bToCNarrowMatch, aToCNarrowMatch, resA, resB);
            }
        }

        // process A closeMatch B
        for (String resA : aToBCloseMatch.keySet()) {
            // search for the following :
            // B exactMatch C -> A closeMatch C
            for (String resB : aToBCloseMatch.get(resA)) {
                // B exactMatch C -> A closeMatch C
                addResToMap(bToCExactMatchMap, aToCCloseMatch, resA, resB);
            }
        }

        // process A broadMatch B
        for (String resA : aToBBroadMatchMap.keySet()){
            // search for the following
            // B exactMatch C -> A broadMatch C
            // B broadMatch C -> A broadMatch C
                for (String resB : aToBBroadMatchMap.get(resA)) {
                    // B exactMatch C -> A broadMatch C
                    addResToMap(bToCExactMatchMap, aToCBroadMatchMap, resA, resB);

                    // B broadMatch C -> A broadMatch C
                    addResToMap(bToCBroadMatchMap, aToCBroadMatchMap, resA, resB);
                }
        }

        // process A narrowMatch B
        for (String resA : aToBNarrowMatch.keySet()){
            // search for the following
            // B exactMatch C -> A narrowMatch C
            // B narrowMatch C -> A narrowMatch C
            for (String resB : aToBNarrowMatch.get(resA)) {
                // B exactMatch C -> A narrowMatch C
                addResToMap(bToCExactMatchMap, aToCNarrowMatch, resA, resB);

                // B narrowMatch C -> A narrowMatch C
                addResToMap(bToCNarrowMatch, aToCNarrowMatch, resA, resB);
            }
        }

        // all possible matching chains have been processed
        alignTaskStructure.saveTaskInfo();
    }


    private void addResToMap(Map<String, List<String>> mapToCheck, Map<String, List<String>> mapToAdd,
                             String resA, String resB) {
        if (mapToCheck.containsKey(resB)) {
            for (String resC : mapToCheck.get(resB)) {
                if (!mapToAdd.containsKey(resA)) {
                    mapToAdd.put(resA, new ArrayList<>());
                }
                if (!mapToAdd.get(resA).contains(resC)) {
                    mapToAdd.get(resA).add(resC);
                }
            }
        }
    }

    private AlignResultStructure createResultStruct() {

        SimpleValueFactory svf = SimpleValueFactory.getInstance();

        float measure = Float.parseFloat("1.0");

        List<Cell> matchCellList = new ArrayList<>();
        // iterate over the A-C maps to construct the

        // A exactMatch C
        for (String resA : aToCExactMatchMap.keySet()) {
            for (String resC : aToCExactMatchMap.get(resA)) {
                Cell cell = new Cell(svf.createIRI(resA), svf.createIRI(resC), measure, EXACT_MATCH_RELATION,
                        SKOS.EXACT_MATCH);
                matchCellList.add(cell);
            }
        }

        // A closeMatch C
        for (String resA : aToCCloseMatch.keySet()) {
            for (String resC : aToCCloseMatch.get(resA)) {
                Cell cell = new Cell(svf.createIRI(resA), svf.createIRI(resC), measure, CLOSE_MATCH_RELATION,
                        SKOS.CLOSE_MATCH);
                matchCellList.add(cell);
            }
        }

        // A narrowMatch C
        for (String resA : aToCNarrowMatch.keySet()) {
            for (String resC : aToCNarrowMatch.get(resA)) {
                Cell cell = new Cell(svf.createIRI(resA), svf.createIRI(resC), measure, NARROR_MATCH_RELATION,
                        SKOS.NARROW_MATCH);
                matchCellList.add(cell);
            }
        }

        // A broadMatch C
        for (String resA : aToCBroadMatchMap.keySet()) {
            for (String resC : aToCBroadMatchMap.get(resA)) {
                Cell cell = new Cell(svf.createIRI(resA), svf.createIRI(resC), measure, BROAD_MATCH_RELATION,
                        SKOS.BROAD_MATCH);
                matchCellList.add(cell);
            }
        }

        return new AlignResultStructure(alignTaskStructure.getTaskInfo(),
                alignTaskStructure.getScenarioDefinition(), matchCellList);

    }


}
