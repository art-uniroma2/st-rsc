package it.uniroma2.art.align;

import io.swagger.model.AlignmentPlan;
import io.swagger.model.ServiceMetadata;
import io.swagger.model.Task;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class MyAlignApiController {
    private static File maindDir = new File("matchTasks");

    public static ResponseEntity<Task> executeDiffTask(AlignmentPlan alignmentPlan) throws IOException {

        AlignTaskManager alignTaskManager = AlignTaskManager.getInstance(maindDir);
        //String randId = matchTaskManager.createDiffStructAndStartTask(neededForMatch);
        Task task = alignTaskManager.createDiffStructAndStartTask(alignmentPlan);

        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    public static ResponseEntity<Void> deleteDiffTask(String taskId) throws IOException {
        AlignTaskManager alignTaskManager = AlignTaskManager.getInstance(maindDir);
        if(alignTaskManager.getIdToMatchTaskStructureMap().containsKey(taskId)){
            alignTaskManager.getIdToMatchTaskStructureMap().remove(taskId);
            File taskDir = new File(maindDir, taskId);
            deleteDir(taskDir);
        } else {
            //the passed task id does not exist
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    private static void deleteDir(File fileOrDirFile) {
        if(fileOrDirFile.isDirectory()){
            for(File file : fileOrDirFile.listFiles()) {
                deleteDir(file);
            }
            //now that all files and dire have been deleted, delete the directory
            fileOrDirFile.delete();
        } else {
            //it is a file, so just delete it
            fileOrDirFile.delete();
        }
    }

    public static ResponseEntity<Resource> getMatchTaskResult(String taskId) throws IOException {
        File taskDir = new File(maindDir, taskId);
        if(!taskDir.exists() || !taskDir.isDirectory()) {
            throw new IOException("taskId is not a valid taskId");
        }
        File taskResultFile = new File(taskDir, AlignTaskStructure.RESULT_FILE_NAME);
        if(!taskResultFile.exists()) {
            throw new IOException("taskId has no results");
        }

        UrlResource urlResource = new UrlResource(taskResultFile.toURI().toURL());
        return new ResponseEntity<>(urlResource, HttpStatus.OK);
    }

    public static ResponseEntity<Task> taskInfoFromId(String taskId) throws IOException {
        AlignTaskManager alignTaskManager = AlignTaskManager.getInstance(maindDir);
        AlignTaskStructure alignTaskStructure = alignTaskManager.getMatchStructFromTaskId(taskId);
        Task task = alignTaskStructure.getTaskInfo();
        return new ResponseEntity<>(task ,HttpStatus.OK);
    }

    public static ResponseEntity<List<Task>> tasksInfo() throws IOException {

        List<Task> taskList = new ArrayList<>();

        AlignTaskManager alignTaskManager = AlignTaskManager.getInstance(maindDir);
        Set<String> taskIdSet = alignTaskManager.getAllTasksIdSet();
        for (String taskId : taskIdSet) {
            taskList.add(alignTaskManager.getMatchStructFromTaskId(taskId).getTaskInfo());
        }
        return new ResponseEntity<>(taskList, HttpStatus.OK);
    }

    public static ResponseEntity<ServiceMetadata> getServiceMetadata() {

        ServiceMetadata serviceMetadata = new ServiceMetadata();
        serviceMetadata.setService("Bootstrapping alignments REST API");
        serviceMetadata.setVersion("1.0");
        serviceMetadata.setStatus(ServiceMetadata.StatusEnum.ACTIVE);
        List<String> specsList = new ArrayList<>();
        specsList.add("http://art.uniroma2.it/maple/alignment-services-3.0.0.yaml");
        serviceMetadata.setSpecs(specsList);
        serviceMetadata.setDocumentation("https://bitbucket.org/art-uniroma2/skosdiff");

        return new ResponseEntity<>(serviceMetadata, HttpStatus.OK);
    }


}
