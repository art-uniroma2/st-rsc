package it.uniroma2.art.align.structures;

import io.swagger.model.ScenarioDefinition;
import io.swagger.model.Task;

import java.util.List;

public class AlignResultStructure {
    private final Task task;
    private final ScenarioDefinition scenarioDefinition;

    private List<Cell> alignCellList;

    public AlignResultStructure(Task task, ScenarioDefinition scenarioDefinition, List<Cell> alignCellList) {
        this.task = task;
        this.scenarioDefinition = scenarioDefinition;
        this.alignCellList = alignCellList;
    }

    public Task getTask() {
        return task;
    }

    public ScenarioDefinition getScenarioDefinition() {
        return scenarioDefinition;
    }

    public List<Cell> getAlignCellList() {
        return alignCellList;
    }
}
