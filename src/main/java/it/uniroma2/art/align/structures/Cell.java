package it.uniroma2.art.align.structures;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.eclipse.rdf4j.model.IRI;

public class Cell {
    private IRI entity1;
    private IRI entity2;
    private float measure;
    private String relation;
    private IRI mappingProperty;

    @JsonCreator
    public Cell(IRI entity1, IRI entity2, float measure, String relation, IRI mappingProperty)  {
        this.entity1 = entity1;
        this.entity2 = entity2;
        this.measure = measure;
        this.relation = relation;
        this.mappingProperty = mappingProperty;
    }

    public IRI getEntity1() {
        return entity1;
    }

    public IRI getEntity2() {
        return entity2;
    }

    public float getMeasure() {
        return measure;
    }

    public String getRelation() {
        return relation;
    }

    public IRI getMappingProperty() {
        return mappingProperty;
    }
}
